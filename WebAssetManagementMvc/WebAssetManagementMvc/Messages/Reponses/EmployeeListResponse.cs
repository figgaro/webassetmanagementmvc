﻿namespace WebAssetManagementMvc.Messages.Reponses
{
    public class EmployeeListResponse
    {

        public int EmployeeId { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }

        public string EmployeeName { get; set; }
        public string Address { get; set; }
        public string Sex { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }


    }
}