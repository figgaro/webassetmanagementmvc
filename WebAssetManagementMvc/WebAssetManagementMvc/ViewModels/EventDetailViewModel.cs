﻿using System.Collections.Generic;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.ViewModels
{
    public class EventDetailViewModel
    {
        public PlanPreventive PlanPreventive { get; set; }
        public PlanCheckList PlanCheckList { get; set; }

        public IList<Employee> Employees { get; set; }
        public IList<Location> Locations { get; set; }
        public IList<Asset> Assets { get; set; }
        public IList<Area> Areas { get; set; }
        public IList<Zone> Zones { get; set; }
        public IList<Status> Statuses { get; set; }
        public IList<Material> Materials { get; set; }
    }
}