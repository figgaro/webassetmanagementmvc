﻿using System;
using System.Dynamic;
using System.Web.UI;
using Newtonsoft.Json;
using WebAssetManagementMvc.Models;
using log4net;

namespace WebAssetManagementMvc.Reports
{
    public partial class ChecklistAirWeek1 : Page
    {
        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        private ILog _log = LogManager.GetLogger(typeof(ChecklistAirWeek1).Name);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) //start save
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                _log.DebugFormat("rowId: {0}", rowId);

                using (var session = SessionFactory.GetNewSession())
                {

                    var checklistAirWeek1 = session.Get<PlanCheckList>(rowId); //มาจากฐานข้อมูลชื่อPlanCheckList
                    if (checklistAirWeek1.FormState != null)
                    {
                        var state = JsonConvert.DeserializeObject<dynamic>(checklistAirWeek1.FormState);
                        _log.DebugFormat("loaded FormStat string: {0}", state);
                        
                        txtRecordDetail.Text = state.RecordDetail;

                        txtDetail1.Text = state.Detail1;
                        txtDetail2.Text = state.Detail2;
                        txtDetail3.Text = state.Detail3;
                        txtDetail4.Text = state.Detail4;

                        txtNotation1.Text = state.Notation1;
                        txtNotation2.Text = state.Notation2;
                        txtNotation3.Text = state.Notayion3;
                        txtNotation4.Text = state.Notation4;

                        cbRepair1.Checked = state.Repair1;
                        cbcbRepair2.Checked = state.Repair2;
                        cbcbRepair3.Checked = state.Repair3;
                        cbcbRepair4.Checked = state.Repair4;

                        cbRagular1.Checked = state.Ragular1;
                        cbRagular2.Checked = state.Ragular2;
                        cbRagular3.Checked = state.Ragular3;
                        cbRagular4.Checked = state.Ragular4;

                        cbChang1.Checked = state.Chang1;
                        cbChang2.Checked = state.Chang2;
                        cbChang3.Checked = state.Chang3;
                        cbChang4.Checked = state.Chang4;
                       
        
                    }
                    lblId.Text = checklistAirWeek1.Id.ToString();
                lblBuilder.Text = "ตึก ict มหาวิทยาลัยพะเยา";
                lblArea.Text = checklistAirWeek1.Area.AreaId;
                lblLocation.Text = checklistAirWeek1.Location.Name;
                lblParamMaterial.Text = checklistAirWeek1.Material.MaterialId;
                lblZone.Text = checklistAirWeek1.Zone.ZoneDetail;
                //lblAsset.Text = ChecklistAirWeek1.Asset.AssetName;
                lblEmployee.Text = checklistAirWeek1.Employee.EmployeeName;
                lblBeginDate.Text = checklistAirWeek1.BeginDate.ToString();
                lblEndDate.Text = checklistAirWeek1.EndDate.ToString();
                lblIsAllDate.Text = checklistAirWeek1.IsAllDay.ToString();


                }

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            dynamic state = new ExpandoObject();

            state.RecordDetail = txtRecordDetail.Text;

            state.Detail1 = txtDetail1.Text;
            state.Detail2 = txtDetail2.Text;
            state.Detail3 = txtDetail3.Text;
            state.Detail4 = txtDetail4.Text;

            state.Notation1 = txtNotation1.Text;
            state.Notation2 = txtNotation2.Text;
            state.Notation3 = txtNotation3.Text;
            state.Notation4 = txtNotation4.Text;

            state.Repair1 = cbRepair1.Checked;
            state.Repair2 = cbcbRepair2.Checked;
            state.Repair3 = cbcbRepair3.Checked;
            state.Repair4 = cbcbRepair4.Checked;

            state.Ragular1 = cbRagular1.Checked;
            state.Ragular2 = cbRagular2.Checked;
            state.Ragular3 = cbRagular3.Checked;
            state.Ragular4 = cbRagular4.Checked;

            state.Chang1 = cbChang1.Checked;
            state.Chang2 = cbChang2.Checked;
            state.Chang3 = cbChang3.Checked;
            state.Chang4 = cbChang4.Checked;


           
           
            var formState = JsonConvert.SerializeObject(state, Formatting.Indented);
            _log.DebugFormat("new formState : {0}", formState);

            using (var session = SessionFactory.GetNewSession())
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                var checklistAirWeek1 = session.Get<PlanCheckList>(rowId);
                checklistAirWeek1.FormState = formState;
                session.Flush();
                _log.DebugFormat("saved id: {0}", rowId);
            }

        }
    }
}