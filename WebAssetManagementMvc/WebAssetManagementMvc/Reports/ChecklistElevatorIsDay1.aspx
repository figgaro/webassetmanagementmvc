﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChecklistElevatorIsDay1.aspx.cs" Inherits="WebAssetManagementMvc.Reports.ChecklistElevatorIsDay1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 140px;
        }
        #frmHead {
            height: 1301px;
            margin-top: 0px;
            margin-bottom: 0px;
            background-color: #FFFFFF;
        }
        .auto-style16 {
            height: 31px;
            width: 82px;
        }
        .auto-style18 {
            width: 113px;
            height: 31px;
        }
        .auto-style23 {
            width: 115px;
            height: 29px;
            margin-left: 960px;
        }
        .auto-style26 {
            width: 113px;
            height: 29px;
        }
        .auto-style27 {
            text-align: right;
            width: 82px;
        }
        .auto-style29 {
            height: 29px;
            text-align: right;
            width: 82px;
        }
        .auto-style31 {
            height: 31px;
            text-align: right;
            width: 123px;
        }
        .auto-style33 {
            height: 29px;
            text-align: right;
            width: 123px;
        }
        .auto-style36 {
            height: 31px;
            width: 115px;
            margin-left: 400px;
        }
        .auto-style42 {
            font-weight: bold;
            text-decoration: underline;
        }
        .auto-style43 {
            font-weight: bold;
        }
        .auto-style46 {
            width: 31px;
            height: 29px;
        }
        .auto-style48 {
            height: 31px;
            width: 31px;
        }
        #TextArea1 {
            top: 858px;
            left: 157px;
            position: absolute;
            height: 89px;
            width: 757px;
        }
        .auto-style50 {
            width: 115px;
            height: 24px;
        }
        .auto-style51 {
            height: 24px;
            text-align: right;
            width: 123px;
        }
        .auto-style52 {
            width: 31px;
            height: 24px;
        }
        .auto-style53 {
            width: 113px;
            height: 24px;
        }
        .auto-style54 {
            height: 24px;
            text-align: right;
            width: 82px;
        }
        .auto-style55 {
            width: 90%;
            font-size: small;
            font-family: "TH SarabunPSK";
            border: 0px solid #000000;
        }
        .auto-style58 {
            text-align: center;
        }
        .auto-style59 {
            text-align: center;
            width: 100px;
        }
        .auto-style61 {
            text-align: center;
            width: 63px;
        }
        .auto-style62 {
            text-align: center;
            width: 63px;
            height: 34px;
        }
        .auto-style63 {
            text-align: center;
            height: 34px;
        }
        .auto-style64 {
            text-align: center;
            width: 100px;
            height: 34px;
        }
        .auto-style65 {
            text-align: center;
            width: 327px;
        }
        .auto-style66 {
            text-align: left;
            height: 34px;
            width: 327px;
        }
        .auto-style67 {
            text-align: center;
            width: 63px;
            height: 36px;
        }
        .auto-style68 {
            text-align: center;
            height: 36px;
        }
        .auto-style69 {
            text-align: left;
            width: 327px;
            height: 36px;
        }
        .auto-style70 {
            text-align: center;
            width: 100px;
            height: 36px;
        }
        .newStyle1 {
            background-color: #EAF5F7;
        }
        .auto-style74 {
            font-size: x-small;
        }
        .auto-style76 {
            font-size: medium;
        }
        .auto-style77 {
            width: 115px;
        }
        .auto-style78 {
            text-align: center;
            width: 72px;
        }
        .auto-style79 {
            text-align: center;
            width: 72px;
            height: 34px;
        }
        .auto-style80 {
            text-align: center;
            width: 72px;
            height: 36px;
        }
        .auto-style82 {
            width: 31px;
        }
        .auto-style83 {
            width: 113px;
        }
        .auto-style84 {
            text-align: right;
            width: 123px;
        }
        .auto-style85 {
            text-align: left;
            width: 327px;
        }
        #txtRecordDetail {
            top: 709px;
            left: 144px;
            position: absolute;
            height: 92px;
            width: 736px;
        }
    </style>
</head>
<body style="border: 1px solid black; width:1000px;height:1300px; margin-bottom: 0px;">
    <form id="frmHead" runat="server">
        
        <asp:Panel ID="Panel1" runat="server" Height="80px" style="text-align: center; margin-top: 32px">
            <asp:Label ID="Label14" runat="server" Text="ช่วงเวลา :" style="top: 71px; left: 19px; position: absolute; height: 19px; width: 85px; font-weight: 700; font-family: 'TH SarabunPSK'; text-align: left;" CssClass="auto-style76"></asp:Label>
            <asp:Label ID="Label15" runat="server" style="font-family: 'TH SarabunPSK'; top: 70px; left: 120px; position: absolute; height: 16px; width: 136px; text-align: right" Text="ประจำสัปดาห์" CssClass="auto-style76"></asp:Label>
            <asp:Label ID="lblNameProject" runat="server" CssClass="auto-style42" Font-Names="TH SarabunPSK" Font-Size="Small" style="font-size: medium; top: 69px; left: 13px; position: absolute; height: 24px; width: 992px;" Text="ระบบบริหารอุปกรณ์ประกอบอาคาร"></asp:Label>
            <asp:Label ID="Label16" runat="server" style="top: 100px; left: 138px; position: absolute; height: 22px; width: 117px; text-align: right; font-family: 'TH SarabunPSK';" Text="ลิฟท์" CssClass="auto-style76"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="Panel3" runat="server" Font-Names="TH SarabunPSK" Height="24px">
            <asp:Label ID="lblAsset" runat="server" Text="ชนิดอุปกรณ์ :" style="top: 98px; left: 18px; position: absolute; height: 26px; width: 109px; font-weight: 700;" Font-Names="TH SarabunPSK" CssClass="auto-style76"></asp:Label>
            <asp:Label ID="lblNameForm" runat="server" Font-Names="TH SarabunPSK" style="font-size: medium; top: 98px; left: 20px; position: absolute; height: 29px; width: 990px; text-align: center; bottom: 424px;" Text="ตารางการตรวจสอบและบำรุงรักษา" CssClass="auto-style42"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="pnlParameter" runat="server" style="top: 149px; left: 12px; position: absolute; height: 190px; width: 997px; font-size: small; bottom: 260px;">
            <table style="width:75%; top: 4px; left: 143px; position: absolute; height: 174px; font-family: 'TH SarabunPSK'; font-size: medium; right: 106px;">
                <tr>
                    <td class="auto-style36">
                        <asp:Label ID="Label1" runat="server" Text="ใบงานเลขที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style31">
                        <asp:Label ID="lblId" runat="server" Text="" style="top: 7px; left: 187px; position: absolute; height: 21px; width: 175px"></asp:Label>
                    </td>
                    <td class="auto-style48"></td>
                    <td class="auto-style18">
                    </td>
                    <td class="auto-style16"></td>
                </tr>
                <tr>
                    <td class="auto-style77">
                        <asp:Label ID="Label2" runat="server" Text="อาคาร :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style84">
                        <asp:Label ID="lblBuilder" runat="server" Text="" style="top: 43px; left: 164px; position: absolute; height: 21px; width: 195px"></asp:Label>
                    </td>
                    <td class="auto-style82"></td>
                    <td class="auto-style83">
                        <asp:Label ID="Label6" runat="server" Text="วัสดุ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style27">
                        <asp:Label ID="lblParamMaterial" runat="server" Text="" style="top: 43px; left: 616px; position: absolute; height: 21px; width: 128px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style23">
                        <asp:Label ID="Label3" runat="server" Text="โซน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style33">
                        <asp:Label ID="lblZone" runat="server" Text="" style="top: 79px; left: 166px; position: absolute; height: 21px; width: 195px"></asp:Label>
                    </td>
                    <td class="auto-style46"></td>
                    <td class="auto-style26">
                        <asp:Label ID="Label7" runat="server" Text="เวลาเริ่มงาน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style29">
                        
                        <asp:Label ID="lblBeginDate" runat="server" Text="" style="top: 79px; left: 640px; position: absolute; height: 21px; width: 106px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style50">
                        <asp:Label ID="Label4" runat="server" Text="สถานที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style51">
                        <asp:Label ID="lblLocation" runat="server" Text="" style="top: 108px; left: 171px; position: absolute; height: 21px; width: 191px"></asp:Label>
                    </td>
                    <td class="auto-style52"></td>
                    <td class="auto-style53">
                        <asp:Label ID="Label8" runat="server" Text="เวลาสิ้นสุด :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style54">
                        <asp:Label ID="lblEndDate" runat="server" Text="" style="top: 108px; left: 630px; position: absolute; height: 21px; width: 114px"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style77">
                        <asp:Label ID="Label5" runat="server" Text="พื้นที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style84">
                        <asp:Label ID="lblArea" runat="server" Text="" style="top: 142px; left: 174px; position: absolute; height: 21px; width: 185px"></asp:Label>
                    </td>
                    <td class="auto-style82"></td>
                    <td class="auto-style83">
                        <asp:Label ID="Label9" runat="server" Text="งานตลอดวัน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style27">
                        <asp:Label ID="lblIsAllDate" runat="server" Text="" style="top: 145px; left: 623px; position: absolute; height: 21px; width: 122px"></asp:Label></td>
                </tr>
            </table>

            

            <asp:TextBox ID="txtRecordDetail" runat="server" ></asp:TextBox>
        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server">
        <asp:Label ID="Label10" runat="server" Text="บันทึกการตรวจสอบ/ซ่อมบำรุง"
            style="top: 819px; left: 159px; position: absolute; height: 19px;
            width: 205px; text-align: justify; font-size: medium; font-family: 'TH SarabunPSK'; right: 661px;"></asp:Label>

        </asp:Panel>

        <asp:Panel ID="Panel4" runat="server" style="top: 982px; left: 694px; position: absolute; height: 68px; width: 223px; text-align: center; font-size: small;">
            <br />
            &nbsp;<asp:Label ID="lblEmployee" runat="server" Font-Names="TH SarabunPSK" style="font-size: medium"></asp:Label>     
            <br />
            <br style="font-size: medium" />
            <asp:Label ID="Label12" runat="server" Text="พนักงานผู้ตรวจเช็ค" Font-Names="TH SarabunPSK" style="font-size: medium"></asp:Label>
        </asp:Panel>

           
        
        
        
        <div style="top: 344px; left: 149px; position: absolute; height: 19px; width: 569px">
            <asp:Label ID="Label13" runat="server" style="font-size: medium; font-family: 'TH SarabunPSK'; font-weight: 700; text-decoration: underline;" Text="CHECKLIST ORDER" Font-Size="Small"></asp:Label>
        </div>
        <div style="top: 375px; left: 129px; position: absolute; height: 419px; width: 804px; font-size: medium;">
            <table align="center" cellspacing="0" class="auto-style55" style="border: 0px solid #000000; height: 205px; width: 93%; font-size: medium;">
                <tr class="auto-style74">
                    <td class="auto-style61" rowspan="2" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">เลขที่        <td class="auto-style78" rowspan="2" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">ข้อกำหนด</td>
                    <td class="auto-style65" rowspan="2" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">รายละเอียด</td>
                    <td class="auto-style58" rowspan="2" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">รายละเอียด การบำรุงรักษา</td>
                    <td class="auto-style58" colspan="3" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">ข้อตรวจพบ/พิจารณา</td>
                    <td class="auto-style59" rowspan="2" style="border: 1px solid #C0C0C0; font-size: medium; font-family: 'TH SarabunPSK';">หมายเหตุ</td>
                </tr>
                <tr class="auto-style74">
                    <td class="auto-style58" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK'; font-size: medium;">ปกติ</td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK'; font-size: medium;">ซ่อม</td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK'; font-size: medium;">เปลี่ยน</td>
                </tr>
                <tr>
                    <td class="auto-style61" style="border: 1px solid #C0C0C0;">1</td>
                    <td class="auto-style78" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style85" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจปุ่มกดทางานถูกต้อง </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail1" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular1" runat="server" Text=" " />
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair1" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange1" runat="server" Text=" " />
                    </td>
                    <td class="auto-style59" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation1" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style62" style="border: 1px solid #C0C0C0;">2</td>
                    <td class="auto-style79" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style66" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจเช็คแผงควบคุม (Switch Box) จะต้องล็อคอยู่ตลอดเวลา </td>
                    <td class="auto-style63" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail2" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style63" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular2" runat="server" Text=" " />
                    </td>
                    <td class="auto-style63" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair2" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style63" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange2" runat="server" Text=" " />
                    </td>
                    <td class="auto-style64" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation2" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style61" style="border: 1px solid #C0C0C0;">3</td>
                    <td class="auto-style78" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style85" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจเช็คแสงสว่างและพัดลมระบายอากาศภายในห้องโดยสาร</td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail3" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular3" runat="server" Text=" " />
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair3" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style58" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange3" runat="server" Text=" " />
                    </td>
                    <td class="auto-style59" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation3" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style67" style="border: 1px solid #C0C0C0;">4</td>
                    <td class="auto-style80" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style69" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจเช็คการทางานของ Safety shoes กับ Door Sensor</td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail4" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular4" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair4" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange4" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation4" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style67" style="border: 1px solid #C0C0C0;">5</td>
                    <td class="auto-style80" style="border: 1px solid #C0C0C0;">สะอาด</td>
                    <td class="auto-style69" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจเช็คธรณีประตู จะต้องไม่มีเศษวัสดุ</td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail5" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular5" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair5" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange5" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation5" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style67" style="border: 1px solid #C0C0C0;">6</td>
                    <td class="auto-style80" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style69" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจเช็คการทางานของโทรศัพท์</td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail6" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular6" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair6" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange6" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation6" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style67" style="border: 1px solid #C0C0C0;">7</td>
                    <td class="auto-style80" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style69" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ทดลองลิฟต์วิ่งขึ้น-ลง ว่าเรียบร้อยดีไม่มีเสียง และไม่สั่น</td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                    <asp:TextBox ID="txtDetail7" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular7" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair7" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange7" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation7" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style67" style="border: 1px solid #C0C0C0;">8</td>
                    <td class="auto-style80" style="border: 1px solid #C0C0C0;">ปกติ </td>
                    <td class="auto-style69" style="border: 1px solid #C0C0C0; font-family: 'TH SarabunPSK';">ตรวจดูกุญแจเปิดประตูลิฟต์</td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0; font-weight: 700;">
                    <asp:TextBox ID="txtDetail8" runat="server"></asp:TextBox>    
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbRagular8" runat="server" Text=" " />
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        
                        <asp:CheckBox ID="cbRepair8" runat="server"  Text=" "/>
                        
                    </td>
                    <td class="auto-style68" style="border: 1px solid #C0C0C0;">
                        <asp:CheckBox ID="cbChange8" runat="server" Text=" " />
                    </td>
                    <td class="auto-style70" style="border: 1px solid #C0C0C0;">
                        <asp:TextBox ID="txtNotation8" runat="server"></asp:TextBox>

                    </td>
                </tr>
                </table>
        </div>

           
        
        
        
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="top: 1135px; left: 858px; position: absolute; height: 26px; width: 56px" Text="บันทึก" />

           
        
        
        
    </form>
</body>
</html>
