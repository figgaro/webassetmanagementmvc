﻿using System;
using System.Dynamic;
using System.Web.UI;
using Newtonsoft.Json;
using WebAssetManagementMvc.Models;
using log4net;

namespace WebAssetManagementMvc.Reports
{
    public partial class BreakdownForm1 : Page
    {

        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        private ILog _log = LogManager.GetLogger(typeof(BreakdownForm1).Name);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) //start save
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                _log.DebugFormat("rowId: {0}", rowId);

                using (var session = SessionFactory.GetNewSession())
                {

                    var notifyBreakDown = session.Get<NotifyBreakDown>(rowId); //มาจากฐานข้อมูลชื่อNotifyBreakDown
                    if (notifyBreakDown.FormState != null)
                    {
                        var state = JsonConvert.DeserializeObject<dynamic>(notifyBreakDown.FormState); //ดูดค่า
                        _log.DebugFormat("loaded FormStat string: {0}", state);
                        txtRecordDetail.Text = state.RecordDetail;
                    }

                    lblParamAssetName.Text = notifyBreakDown.Asset.AssetName;
                    lblId.Text = notifyBreakDown.Id.ToString();
                    lblBuilder.Text = "ตึก ict มหาวิทยาลัยพะเยา";
                    lblArea.Text = notifyBreakDown.Area.AreaId;
                    lblDateNotify.Text = notifyBreakDown.NotifyDate.ToString(DATE_FORMAT);
                    lblEmpBearer.Text = notifyBreakDown.EmployeeBearer.EmployeeName;
                    lblEmpImformant.Text = notifyBreakDown.EmployeeInformant.EmployeeName;
                    lblLocation.Text = notifyBreakDown.Location.Name;
                    lblParamMaterial.Text = notifyBreakDown.Material.MaterialId;
                    lblZone.Text = notifyBreakDown.Zone.ZoneDetail;
                    lblSymptomBefore.Text = notifyBreakDown.SymptomBefore;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            dynamic state = new ExpandoObject();
            state.RecordDetail = txtRecordDetail.Text;
           
            var formState = JsonConvert.SerializeObject(state, Formatting.Indented);
            _log.DebugFormat("new formState : {0}", formState);

            using (var session = SessionFactory.GetNewSession())
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                var notifyBreakDown = session.Get<NotifyBreakDown>(rowId);
                notifyBreakDown.FormState = formState;
                session.Flush();
                _log.DebugFormat("saved id: {0}", rowId);
            }


        }//end method
    }
}