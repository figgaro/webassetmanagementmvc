﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Reports
{
    public partial class PmAir6Month1 : System.Web.UI.Page
    {
         private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
         private ILog _log = LogManager.GetLogger(typeof(PmAir6Month1).Name);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) //start save
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                _log.DebugFormat("rowId: {0}", rowId);

                using (var session = SessionFactory.GetNewSession())
                {

                    var pmAir6Month1 = session.Get<PlanPreventive>(rowId); //มาจากฐานข้อมูลชื่อPlanCheckList
                    if (pmAir6Month1.FormState != null)
                    {
                        var state = JsonConvert.DeserializeObject<dynamic>(pmAir6Month1.FormState);
                        _log.DebugFormat("loaded FormStat string: {0}", state);

                        txtRecordDetail.Text = state.RecordDetail;

                        txtDetail1.Text = state.Detail1;
                        txtDetail2.Text = state.Detail2;
                        txtDetail3.Text = state.Detail3;
                        txtDetail4.Text = state.Detail4;
                        txtDetail5.Text = state.Detail5;
                        txtDetail6.Text = state.Detail6;
                        txtDetail7.Text = state.Detail7;
                        txtDetail8.Text = state.Detail8;
                        txtDetail9.Text = state.Detail9;
                        txtDetail10.Text = state.Detail10;
                        txtDetail11.Text = state.Detail11;
                        txtDetail12.Text = state.Detail12;
                        txtDetail13.Text = state.Detail13;
                        txtDetail14.Text = state.Detail14;
                        txtDetail15.Text = state.Detail15;
                        txtDetail16.Text = state.Detail16;

                        cbRagular1.Checked = state.Ragular1;
                        cbRagular2.Checked = state.Ragular2;
                        cbRagular3.Checked = state.Ragular3;
                        cbRagular4.Checked = state.Ragular4;
                        cbRagular5.Checked = state.Ragular5;
                        cbRagular6.Checked = state.Ragular6;
                        cbRagular7.Checked = state.Ragular7;
                        cbRagular8.Checked = state.Ragular8;
                        cbRagular9.Checked = state.Ragular9;
                        cbRagular10.Checked = state.Ragular10;
                        cbRagular11.Checked = state.Ragular11;
                        cbRagular12.Checked = state.Ragular12;
                        cbRagular13.Checked = state.Ragular13;
                        cbRagular14.Checked = state.Ragular14;
                        cbRagular15.Checked = state.Ragular15;
                        cbRagular16.Checked = state.Ragular16;

                        cbRepair1.Checked = state.Repair1;
                        cbRepair2.Checked = state.Repair2;
                        cbRepair3.Checked = state.Repair3;
                        cbRepair4.Checked = state.Repair4;
                        cbRepair5.Checked = state.Repair5;
                        cbRepair6.Checked = state.Repair6;
                        cbRepair7.Checked = state.Repair7;
                        cbRepair8.Checked = state.Repair8;
                        cbRepair9.Checked = state.Repair9;
                        cbRepair10.Checked = state.Repair10;
                        cbRepair11.Checked = state.Repair11;
                        cbRepair12.Checked = state.Repair12;
                        cbRepair13.Checked = state.Repair13;
                        cbRepair14.Checked = state.Repair14;
                        cbRepair15.Checked = state.Repair15;
                        cbRepair16.Checked = state.Repair16;

                        cbChange1.Checked = state.Change1;
                        cbChange2.Checked = state.Change2;
                        cbChange3.Checked = state.Change3;
                        cbChange4.Checked = state.Change4;
                        cbChange5.Checked = state.Change5;
                        cbChange6.Checked = state.Change6;
                        cbChange7.Checked = state.Change7;
                        cbChange8.Checked = state.Change8;
                        cbChange9.Checked = state.Change9;
                        cbChange10.Checked = state.Change10;
                        cbChange11.Checked = state.Change11;
                        cbChange12.Checked = state.Change12;
                        cbChange13.Checked = state.Change13;
                        cbChange14.Checked = state.Change14;
                        cbChange15.Checked = state.Change15;
                        cbChange16.Checked = state.Change16;

                        txtNotation1.Text = state.Notation1;
                        txtNotation2.Text = state.Notation2;
                        txtNotation3.Text = state.Notation3;
                        txtNotation4.Text = state.Notation4;
                        txtNotation5.Text = state.Notation5;
                        txtNotation6.Text = state.Notation6;
                        txtNotation7.Text = state.Notation7;
                        txtNotation8.Text = state.Notation8;
                        txtNotation9.Text = state.Notation9;
                        txtNotation10.Text = state.Notation10;
                        txtNotation11.Text = state.Notation11;
                        txtNotation12.Text = state.Notation12;
                        txtNotation13.Text = state.Notation13;
                        txtNotation14.Text = state.Notation14;
                        txtNotation15.Text = state.Notation15;
                        txtNotation16.Text = state.Notation16;


                    }
                    lblId.Text = pmAir6Month1.Id.ToString();
                    lblBuilder.Text = "ตึก ict มหาวิทยาลัยพะเยา";
                    lblArea.Text = pmAir6Month1.Area.AreaId;
                    lblLocation.Text = pmAir6Month1.Location.Name;
                    lblParamMaterial.Text = pmAir6Month1.Material.MaterialId;
                    lblZone.Text = pmAir6Month1.Zone.ZoneDetail;
                    //lblAsset.Text = ChecklistAirWeek1.Asset.AssetName;
                    lblEmployee.Text = pmAir6Month1.Employee.EmployeeName;
                    lblBeginDate.Text = pmAir6Month1.BeginDate.ToString();
                    lblEndDate.Text = pmAir6Month1.EndDate.ToString();
                    lblIsAllDate.Text = pmAir6Month1.IsAllDay.ToString();

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            dynamic state = new ExpandoObject();

            state.RecordDetail = txtRecordDetail.Text;

            state.Detail1 = txtDetail1.Text;
            state.Detail2 = txtDetail2.Text;
            state.Detail3 = txtDetail3.Text;
            state.Detail4 = txtDetail4.Text;
            state.Detail5 = txtDetail5.Text;
            state.Detail6 = txtDetail6.Text;
            state.Detail7 = txtDetail7.Text;
            state.Detail8 = txtDetail8.Text;
            state.Detail9 = txtDetail9.Text;
            state.Detail10 = txtDetail10.Text;
            state.Detail11 = txtDetail11.Text;
            state.Detail12 = txtDetail12.Text;
            state.Detail13 = txtDetail13.Text;
            state.Detail14 = txtDetail14.Text;
            state.Detail15 = txtDetail15.Text;
            state.Detail16 = txtDetail16.Text;

            state.Ragular1 = cbRagular1.Checked;
            state.Ragular2 = cbRagular2.Checked;
            state.Ragular3 = cbRagular3.Checked;
            state.Ragular4 = cbRagular4.Checked;
            state.Ragular5 = cbRagular5.Checked;
            state.Ragular6 = cbRagular6.Checked;
            state.Ragular7 = cbRagular7.Checked;
            state.Ragular8 = cbRagular8.Checked;
            state.Ragular9 = cbRagular9.Checked;
            state.Ragular10 = cbRagular10.Checked;
            state.Ragular11 = cbRagular11.Checked;
            state.Ragular12 = cbRagular12.Checked;
            state.Ragular13 = cbRagular13.Checked;
            state.Ragular14 = cbRagular14.Checked;
            state.Ragular15 = cbRagular15.Checked;
            state.Ragular16 = cbRagular16.Checked;

            state.Repair1 = cbRepair1.Checked;
            state.Repair2 = cbRepair2.Checked;
            state.Repair3 = cbRepair3.Checked;
            state.Repair4 = cbRepair4.Checked;
            state.Repair5 = cbRepair5.Checked;
            state.Repair6 = cbRepair6.Checked;
            state.Repair7 = cbRepair7.Checked;
            state.Repair8 = cbRepair8.Checked;
            state.Repair9 = cbRepair9.Checked;
            state.Repair10 = cbRepair10.Checked;
            state.Repair11 = cbRepair11.Checked;
            state.Repair12 = cbRepair12.Checked;
            state.Repair13 = cbRepair13.Checked;
            state.Repair14 = cbRepair14.Checked;
            state.Repair15 = cbRepair15.Checked;
            state.Repair16 = cbRepair16.Checked;

            state.Change1 = cbChange1.Checked;
            state.Change2 = cbChange2.Checked;
            state.Change3 = cbChange3.Checked;
            state.Change4 = cbChange4.Checked;
            state.Change5 = cbChange5.Checked;
            state.Change6 = cbChange6.Checked;
            state.Change7 = cbChange7.Checked;
            state.Change8 = cbChange8.Checked;
            state.Change9 = cbChange9.Checked;
            state.Change10 = cbChange10.Checked;
            state.Change11 = cbChange11.Checked;
            state.Change12 = cbChange12.Checked;
            state.Change13 = cbChange13.Checked;
            state.Change14 = cbChange14.Checked;
            state.Change15 = cbChange15.Checked;
            state.Change16 = cbChange16.Checked;

            state.Notation1 = txtNotation1.Text;
            state.Notation2 = txtNotation2.Text;
            state.Notation3 = txtNotation3.Text;
            state.Notation4 = txtNotation4.Text;
            state.Notation5 = txtNotation5.Text;
            state.Notation6 = txtNotation6.Text;
            state.Notation7 = txtNotation7.Text;
            state.Notation8 = txtNotation8.Text;
            state.Notation9 = txtNotation9.Text;
            state.Notation10 = txtNotation10.Text;
            state.Notation11 = txtNotation11.Text;
            state.Notation12 = txtNotation12.Text;
            state.Notation13 = txtNotation13.Text;
            state.Notation14 = txtNotation14.Text;
            state.Notation15 = txtNotation15.Text;
            state.Notation16 = txtNotation16.Text;


            var formState = JsonConvert.SerializeObject(state, Formatting.Indented);
            _log.DebugFormat("new formState : {0}", formState);

            using (var session = SessionFactory.GetNewSession())
            {
                var rowId = int.Parse(Request.QueryString["rowId"]);
                var pmAir6Month1 = session.Get<PlanPreventive>(rowId);
                pmAir6Month1.FormState = formState;
                session.Flush();
                _log.DebugFormat("saved id: {0}", rowId);
            }

        }

        

        }//end method
    }

