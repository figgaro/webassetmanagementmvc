﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BreakdownForm1.aspx.cs" Inherits="WebAssetManagementMvc.Reports.BreakdownForm1" %>
<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BreakdownForm.aspx.cs" Inherits="WebAssetManagementMvc.BreakdownForm1" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 140px;
        }
        #frmHead {
            height: 839px;
            background-color: #FFFFFF;
        }
        .auto-style42 {
            font-weight: bold;
            text-decoration: underline;
        }
        .auto-style43 {
            font-weight: bold;
        }
        .auto-style51 {
            width: 147px;
            height: 18px;
        }
        .auto-style52 {
            height: 18px;
            text-align: right;
            width: 131px;
        }
        .auto-style53 {
            height: 18px;
        }
        .auto-style54 {
            width: 218px;
            height: 18px;
        }
        .auto-style67 {
            width: 163px;
            height: 18px;
        }
        .auto-style72 {
            height: 9px;
            text-align: right;
            width: 163px;
        }
        .auto-style73 {
            width: 147px;
            height: 9px;
        }
        .auto-style74 {
            height: 9px;
            text-align: right;
            width: 131px;
        }
        .auto-style75 {
            height: 9px;
        }
        .auto-style76 {
            width: 218px;
            height: 9px;
        }
        .auto-style77 {
            width: 147px;
            height: 28px;
        }
        .auto-style78 {
            text-align: right;
            width: 131px;
            height: 28px;
        }
        .auto-style80 {
            width: 218px;
            height: 28px;
        }
        .auto-style81 {
            text-align: right;
            width: 163px;
            height: 28px;
        }
        .auto-style82 {
            width: 147px;
            height: 19px;
        }
        .auto-style83 {
            height: 19px;
            text-align: right;
            width: 131px;
        }
        .auto-style84 {
            height: 19px;
        }
        .auto-style85 {
            width: 218px;
            height: 19px;
        }
        .auto-style86 {
            height: 19px;
            text-align: right;
            width: 163px;
        }
        .auto-style87 {
            height: 16px;
            text-align: right;
            width: 163px;
        }
        .auto-style88 {
            width: 147px;
            height: 16px;
        }
        .auto-style89 {
            height: 16px;
            text-align: right;
            width: 131px;
        }
        .auto-style90 {
            height: 16px;
        }
        .auto-style91 {
            width: 218px;
            height: 16px;
        }
        .auto-style92 {
            height: 28px;
        }
    </style>
</head>
<body style="width:1000px;height:842px;border-color:black;border-width:1px;border-style:solid">
    <form id="frmHead" runat="server">
        
        <asp:Panel ID="Panel1" runat="server" Height="24px" style="text-align: center; margin-top: 32px">
            <asp:Label ID="lblNameProject" runat="server" Text="ระบบบริหารอุปกรณ์ประกอบอาคาร" Font-Names="TH SarabunPSK" Font-Size="Small" style="font-size: medium; top: 48px; left: 14px; position: absolute; height: 19px; width: 997px;" CssClass="auto-style42"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="Panel3" runat="server" Font-Names="TH SarabunPSK" Height="24px">
            <asp:Label ID="lblAssetName" runat="server" Text="ชนิดอุปกรณ์ :" style="font-size: medium; top: 79px; left: 14px; position: absolute; height: 17px; width: 109px; font-weight: 700;"></asp:Label>
            <asp:Label ID="lblNameForm" runat="server" Font-Names="TH SarabunPSK" style="font-size: medium; top: 76px; left: 14px; position: absolute; height: 16px; width: 992px; text-align: center; bottom: 452px;" Text="แบบบันทึกการตรวจสอบและบำรุงรักษาอุปกรณ์ Break Down" CssClass="auto-style42"></asp:Label>
            <asp:Label ID="lblParamAssetName" runat="server" style="top: 79px; left: 128px; position: absolute; height: 18px; width: 110px; font-size: medium;"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="pnlParameter" runat="server" style="top: 166px; left: 12px; position: absolute; height: 169px; width: 1002px; font-size: small; bottom: 216px;">
            <table style="width:55%; top: -1px; left: 254px; position: absolute; height: 168px; font-family: 'TH SarabunPSK'; background-color: #FFFFFF; font-size: medium;">
                <tr>
                    <td class="auto-style51">
                        <asp:Label ID="Label1" runat="server" Text="ใบงานเลขที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style52">
                        <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style53"></td>
                    <td class="auto-style54">
                    </td>
                    <td class="auto-style67"></td>
                </tr>
                <tr>
                    <td class="auto-style88">
                        <asp:Label ID="Label2" runat="server" Text="อาคาร :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style89">
                        <asp:Label ID="lblBuilder" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style90"></td>
                    <td class="auto-style91">
                        <asp:Label ID="Label6" runat="server" Text="วัสดุ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style87">
                        <asp:Label ID="lblParamMaterial" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style82">
                        <asp:Label ID="Label3" runat="server" Text="โซน :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style83">
                        <asp:Label ID="lblZone" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style84"></td>
                    <td class="auto-style85">
                        <asp:Label ID="Label7" runat="server" Text="อาการที่เสีย :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style86">
                        <asp:Label ID="lblSymptomBefore" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style77">
                        <asp:Label ID="Label4" runat="server" Text="สถานที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style78">
                        <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style92"></td>
                    <td class="auto-style80">
                        <asp:Label ID="Label8" runat="server" Text="วัน/เวลาที่แจ้งซ่อม :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style81">
                        <asp:Label ID="lblDateNotify" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style73">
                        <asp:Label ID="Label5" runat="server" Text="พื้นที่ :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style74">
                        <asp:Label ID="lblArea" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="auto-style75"></td>
                    <td class="auto-style76">
                        <asp:Label ID="Label9" runat="server" Text="ผู้แจ้งซ่อม :" CssClass="auto-style43"></asp:Label>
                    </td>
                    <td class="auto-style72">
                        <asp:Label ID="lblEmpImformant" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>

        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server">

            <asp:TextBox ID="txtRecordDetail" runat="server" BackColor="#FFFFCC" style="top: 402px;
            left: 265px; position: absolute; height: 159px; width: 542px; background-color: #FFFFFF;" TextMode="MultiLine"></asp:TextBox>

        </asp:Panel>

        <asp:Panel ID="Panel4" runat="server" style="top: 628px; left: 624px; position: absolute; height: 62px; width: 177px; text-align: center; font-size: small; background-color: #FFFFFF; right: 302px;">
            &nbsp;<asp:Label ID="lblEmpBearer" runat="server" Font-Names="TH SarabunPSK" style="text-decoration: underline; font-size: medium;"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label12" runat="server" Font-Names="TH SarabunPSK" style="font-size: medium" Text="พนักงานซ่อมบำรุง"></asp:Label>
        </asp:Panel>

           
        
        
        
        <div style="top: 369px; left: 267px; position: absolute; height: 19px; width: 456px; font-size: medium; text-decoration: underline;">
            บันทึกการตรวจสอบ/ซ่อมบำรุง
        </div>

           
        
        
        
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="top: 732px; left: 743px; position: absolute; height: 26px; width: 56px" Text="บันทึก" />

           
        
        
        
    </form>
</body>
</html>

