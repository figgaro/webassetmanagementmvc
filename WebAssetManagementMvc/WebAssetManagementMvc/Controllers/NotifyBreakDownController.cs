﻿using System;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using NHibernate.Linq;
using Newtonsoft.Json;
using WebAssetManagementMvc.Helpers;
using WebAssetManagementMvc.Models;
using log4net;
using Position = WebAssetManagementMvc.Models.Position;


namespace WebAssetManagementMvc.Controllers
{
    public class NotifyBreakDownController : Controller
    {
        private ILog _log = LogManager.GetLogger(typeof(NotifyBreakDownController).Name);

        private const string DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

        public NotifyBreakDownController()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

        }

        [HttpGet]
        public ActionResult AddNotifyBreakDown(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);
            try
            {
                AddNotifyBreakDownViewModel model;
                using (var session = SessionFactory.GetNewSession())
                {

                    model = new AddNotifyBreakDownViewModel()
                    {
                        Areas = session.Query<Area>().ToList(),
                        Assets = session.Query<Asset>().ToList(),
                        Employees = session.Query<Employee>().ToList(),
                        Locations = session.Query<Location>().ToList(),
                        Materials = session.Query<Material>().ToList(),
                        Statuses = session.Query<Status>().ToList(),
                        Zones = session.Query<Zone>().ToList(),
                    };

                    view.Model = model;
                }


            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
            return view;
        }


        [HttpPost]
        public ActionResult AddNotifyBreakDown(NotifyBreakDown notifyBreakDown)
        {
            try
            {

                var notifyDate = Request.Form["NotifyDate"].ToDate(DATE_FORMAT);
                notifyBreakDown.NotifyDate = notifyDate.Value;

                using (var session = SessionFactory.GetNewSession())
                {
                    _log.DebugFormat("notifyBreakDown.Material.MaterialId: {0}", notifyBreakDown.Material.MaterialId);
                    _log.DebugFormat("notifyBreakDown.Asset.AssetId: {0}", notifyBreakDown.Asset.AssetId);
                    _log.DebugFormat("notifyBreakDown.Zone.ZoneId: {0}", notifyBreakDown.Zone.ZoneId);
                    _log.DebugFormat("notifyBreakDown.Location.LocationId: {0}", notifyBreakDown.Location.LocationId);
                    _log.DebugFormat("notifyBreakDown.Area.AreaId: {0}", notifyBreakDown.Area.AreaId);
                    _log.DebugFormat("notifyBreakDown.EmployeeInformant.EmployeeId: {0}", notifyBreakDown.EmployeeInformant.EmployeeId);

                    session.Save(notifyBreakDown);
                    session.Flush();
                }

                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Saved successfully",
                                     Html = string.Format("Saved new NotifyBreakDown id: {0}", notifyBreakDown.Id),
                                     AutoHide = true,
                                 }).Show();

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();


            }


            return this.Direct();
        }


        [HttpGet]
        public ActionResult NotifyBreakDownList(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);

            try
            {

                NotifyBreakDownListViewModel model;
                using (var session = SessionFactory.GetNewSession())
                {

                    model =
                        new NotifyBreakDownListViewModel()
                        {
                            Areas = session.Query<Area>().ToList(),
                            Assets = session.Query<Asset>().ToList(),
                            Employees = session.Query<Employee>().ToList(),
                            Locations = session.Query<Location>().ToList(),
                            Materials = session.Query<Material>().ToList(),
                            Statuses = session.Query<Status>().ToList(),
                            Zones = session.Query<Zone>().ToList(),

                            NotifyBreakDowns = session.Query<NotifyBreakDown>()
                            .Select(n => new NotifyBreakDonwListRecordViewModel()
                            {
                                AreaName = n.Area.Detail,
                                AssetName = n.Asset.AssetName,
                                EmployeeInformantName = n.EmployeeInformant.EmployeeName,
                                EmployeeBearerId = n.EmployeeBearer != null ? n.EmployeeBearer.EmployeeId : 0,
                                EmployeeBearerName = n.EmployeeBearer != null ? n.EmployeeBearer.EmployeeName : "",
                                Id = n.Id,
                                LocationName = n.Location.Name,
                                MaintenanceDate = n.MaintenanceDate.HasValue ? n.MaintenanceDate.Value.ToString(DATE_FORMAT) : "",
                                MaterialName = n.Material.MaterialId,
                                NotifyDate = n.NotifyDate.ToString(DATE_FORMAT),
                                StatusId = n.Status.StatusId,
                                StatusName = n.Status.StatusName,
                                SymptomAfter = string.IsNullOrEmpty(n.SymptomAfter) ? "" : n.SymptomAfter,
                                SymptomBefore = n.SymptomBefore,
                                Title = n.Title,
                                ZoneName = n.Zone.ZoneDetail,
                            }).ToList()
                        };
                    //for drop down
                    model.Employees.Add(new Employee() { EmployeeId = 0, EmployeeName = "", Position = new Position()});


                    _log.DebugFormat("Area count: {0}", model.Areas.Count());
                    _log.DebugFormat("Asset count: {0}", model.Assets.Count());
                    _log.DebugFormat("Employee count: {0}", model.Employees.Count());

                    _log.DebugFormat("Location count: {0}", model.Locations.Count());
                    _log.DebugFormat("Material count: {0}", model.Materials.Count());
                    _log.DebugFormat("Status count: {0}", model.Statuses.Count());
                    _log.DebugFormat("Zone count: {0}", model.Zones.Count());

                    view.Model = model;
                    return view;
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);

                return view;
            }

        }


        [HttpGet]
        public ActionResult NotifyBreakDownHistoryList(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);

            try
            {

                NotifyBreakDownListViewModel model;
                using (var session = SessionFactory.GetNewSession())
                {

                    model =
                        new NotifyBreakDownListViewModel()
                        {
                            Areas = session.Query<Area>().ToList(),
                            Assets = session.Query<Asset>().ToList(),
                            Employees = session.Query<Employee>().ToList(),
                            Locations = session.Query<Location>().ToList(),
                            Materials = session.Query<Material>().ToList(),
                            Statuses = session.Query<Status>().ToList(),
                            Zones = session.Query<Zone>().ToList(),
                            NotifyBreakDowns = session.Query<NotifyBreakDown>()
                            .Select(n => new NotifyBreakDonwListRecordViewModel()
                            {
                                AreaName = n.Area.Detail,
                                AssetName = n.Asset.AssetName,
                                EmployeeInformantName = n.EmployeeInformant.EmployeeName,
                                EmployeeBearerId = n.EmployeeBearer != null ? n.EmployeeBearer.EmployeeId : 0,
                                EmployeeBearerName = n.EmployeeBearer != null ? n.EmployeeBearer.EmployeeName : "",
                                Id = n.Id,
                                LocationName = n.Location.Name,
                                //default to now
                                MaintenanceDate = n.MaintenanceDate.HasValue ?
                                n.MaintenanceDate.Value.ToString(DATE_FORMAT) : DateTime.Now.ToString(DATE_FORMAT),
                                MaterialName = n.Material.MaterialId,
                                NotifyDate = n.NotifyDate.ToString(DATE_FORMAT),
                                StatusId = n.Status.StatusId,
                                StatusName = n.Status.StatusName,
                                SymptomAfter = string.IsNullOrEmpty(n.SymptomAfter) ? "" : n.SymptomAfter,
                                SymptomBefore = n.SymptomBefore,
                                Title = n.Title,
                                ZoneName = n.Zone.ZoneDetail,
                            }).ToList()
                        };
                    model.Employees.Add(new Employee() { EmployeeId = 0, EmployeeName = "", Position = new Position()});
                    view.Model = model;
                    return view;
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);

                return view;
            }

        }



        [HttpPut]
        public ActionResult EditNotifyBreakDown(int id, string field, string oldValue, string newValue)
        {

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new DateTimeConverter("dd/MM/yyyy HH:mm:ss"));
            var notifyBreakDown =
                JsonConvert.DeserializeObject<NotifyBreakDonwListRecordViewModel>(Request["notifyBreakDown"], settings);

            _log.InfoFormat("StatusId: {0}", notifyBreakDown.StatusId);

            var message = "<b>Property:</b> {0}<br /><b>Field:</b> {1}<br /><b>Old Value:</b> {2}<br /><b>New Value:</b> {3}";
            using (var session = SessionFactory.GetNewSession())
            {

                var notifyBreakDownFromDb = session.Get<NotifyBreakDown>(id);
                notifyBreakDownFromDb.SymptomAfter = notifyBreakDown.SymptomAfter;

                if (notifyBreakDown.StatusId != 0)
                {
                    notifyBreakDownFromDb.Status = new Status() { StatusId = notifyBreakDown.StatusId };
                }

                if (notifyBreakDown.EmployeeBearerId != 0)
                {
                    notifyBreakDownFromDb.EmployeeBearer = new Employee() { EmployeeId = notifyBreakDown.EmployeeBearerId };
                }
                _log.DebugFormat("notifyBreakDown.MaintenanceDate: {0}", notifyBreakDown.MaintenanceDate);
                if (!string.IsNullOrEmpty(notifyBreakDown.MaintenanceDate))
                {
                    notifyBreakDownFromDb.MaintenanceDate = DateTimeHelper.ToDate(notifyBreakDown.MaintenanceDate, DATE_FORMAT);
                }

                session.Flush();
            }

            // Send Message...
            X.Msg.Notify(new NotificationConfig()
            {
                Title = "Edit Record #" + id.ToString(),
                Html = string.Format(message, id, field, oldValue, newValue),
                Width = 250
            }).Show();

            var store = X.GetCmp<Store>("notifyBreakDownStore");
            store.GetById(id).Commit();

            return this.Direct();
        }
    }
}
