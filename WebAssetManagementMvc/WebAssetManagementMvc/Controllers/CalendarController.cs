﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using NHibernate.Linq;
using Newtonsoft.Json;
using WebAssetManagementMvc.Helpers;
using WebAssetManagementMvc.Models;
using WebAssetManagementMvc.ViewModels;
using log4net;

namespace WebAssetManagementMvc.Controllers
{
    public class CalendarController : Controller
    {
        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";

        private readonly ILog _log = LogManager.GetLogger(typeof(CalendarController).Name);

        public Ext.Net.MVC.PartialViewResult CalendarView(string containerId)
        {
            Ext.Net.X.Head().AddScript("var a =1;");
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, Ext.Net.RenderMode.AddTo);
            return view;
        }




        public ActionResult GetAllEvents()
        {
            using (var session = SessionFactory.GetNewSession())
            {
                try
                {
                    var preventives = session.Query<PlanPreventive>()
                        .ToList()
                        .Select(ev => new CalendarEvent()
                                          {
                                              id = string.Format("PP:{0}", ev.Id),
                                              title = ev.Title,
                                              start = ev.BeginDate,
                                              end = ev.EndDate,
                                              allDay = ev.IsAllDay,
                                              type = "PlanPreventive",
                                              rowId = ev.Id,
                                          });

                    var checkLists = session.Query<PlanCheckList>()
                        .ToList()
                        .Select(ev => new CalendarEvent()
                                          {
                                              id = string.Format("PC:{0}", ev.Id),
                                              title = ev.Title,
                                              start = ev.BeginDate,
                                              end = ev.EndDate,
                                              allDay = ev.IsAllDay,
                                              type = "PlanCheckList",
                                              rowId = ev.Id,
                                          });
                    _log.Debug("data sent back to client");
                    var allEvents = preventives.Concat(checkLists);

                    return new CustomJsonResult(allEvents);
                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                    return Content(ex.Message);
                }

            }
        }


        public Ext.Net.MVC.PartialViewResult ShowAddEventWindow(string containerId)
        {

            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.RenderTo);


            try
            {
                var beginDate = Request.QueryString["beginDate"].ToDate(DATE_FORMAT);
                var endDate = Request.QueryString["endDate"].ToDate(DATE_FORMAT);
                var isAllDay = Request.QueryString["isAllDay"].ToBoolean();

                var selectedEvent = new CalendarEvent()
                                        {
                                            start = beginDate.Value,
                                            end = endDate,
                                            allDay = isAllDay,
                                        };

                Session["selectedEvent"] = selectedEvent;
                var model = new AddEventViewModel();
                using (var session = SessionFactory.GetNewSession())
                {
                    model.Employees = session.Query<Employee>().ToList();
                    model.Locations = session.Query<Location>().ToList();
                    model.Assets = session.Query<Asset>().ToList();

                    model.Areas = session.Query<Area>().ToList();
                    model.Zones = session.Query<Zone>().ToList();
                    model.Statuses = session.Query<Status>().ToList();
                    model.Materials = session.Query<Material>().ToList();
                }

                view.Model = model;

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }

            return view;
        }

        public DirectResult LoadSelectedEvent()
        {


            var addPreventiveFormBeginDate = X.GetCmp<TextField>("AddPreventiveFormBeginDate");
            var addPreventiveFormEndDate = X.GetCmp<TextField>("AddPreventiveFormEndDate");
            var addPreventiveFormIsAllDay = X.GetCmp<TextField>("AddPreventiveFormIsAllDay");

            var addCheckListFormBeginDate = X.GetCmp<TextField>("AddCheckListFormBeginDate");
            var addCheckListFormEndDate = X.GetCmp<TextField>("AddCheckListFormEndDate");
            var addCheckListFormIsAllDay = X.GetCmp<TextField>("AddCheckListFormIsAllDay");

            var selectedEvent = (CalendarEvent)Session["selectedEvent"];


            addPreventiveFormBeginDate.SetValue(selectedEvent.start.ToString(DATE_FORMAT));
            addPreventiveFormEndDate.SetValue(selectedEvent.end.Value.ToString(DATE_FORMAT));
            addPreventiveFormIsAllDay.SetValue(selectedEvent.allDay);

            addCheckListFormBeginDate.SetValue(selectedEvent.start.ToString(DATE_FORMAT));
            addCheckListFormEndDate.SetValue(selectedEvent.end.Value.ToString(DATE_FORMAT));
            addCheckListFormIsAllDay.SetValue(selectedEvent.allDay);



            //set value
            return this.Direct();
        }


        [HttpPost]
        public DirectResult AddPlanPreventive(PlanPreventive preventive)
        {
            try
            {

                var beginDate = Request.Form["BeginDate"].ToDate(DATE_FORMAT);
                var endDate = Request.Form["EndDate"].ToDate(DATE_FORMAT);
                var isAllDay = Request.Form["IsAllDay"].ToBoolean();

                preventive.BeginDate = beginDate.Value;
                preventive.EndDate = endDate;
                preventive.IsAllDay = isAllDay;



                using (var session = SessionFactory.GetNewSession())
                {
                    _log.DebugFormat("employeeId: {0}", preventive.Employee.EmployeeId);
                    _log.DebugFormat("locationId: {0}", preventive.Location.LocationId);
                    _log.DebugFormat("zoneId: {0}", preventive.Zone.ZoneId);

                    _log.DebugFormat("assetId: {0}", preventive.Asset.AssetId);
                    _log.DebugFormat("areaId: {0}", preventive.Area.AreaId);
                    _log.DebugFormat("statusId: {0}", preventive.Status.StatusId);
                    _log.DebugFormat("materialId: {0}", preventive.Material.MaterialId);

                    session.Save(preventive);
                    session.Flush();
                }

                //reload all event
                X.AddScript("calendar.fullCalendar( 'refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Saved successfully",
                                     Html = string.Format("Saved new Preventive id: {0}", preventive.Id),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                X.Msg.Alert("error", "Error adding event, please fill all data and try again.");

                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error Preventive new CheckList",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }


            return this.Direct();
        }


        [HttpPost]
        public DirectResult AddPlanCheckList(PlanCheckList checkList)
        {
            try
            {

                var beginDate = Request.Form["BeginDate"].ToDate(DATE_FORMAT);
                var endDate = Request.Form["EndDate"].ToDate(DATE_FORMAT);
                var isAllDay = Request.Form["IsAllDay"].ToBoolean();

                checkList.BeginDate = beginDate.Value;
                checkList.EndDate = endDate;
                checkList.IsAllDay = isAllDay;

                using (var session = SessionFactory.GetNewSession())
                {
                    session.Save(checkList);
                    session.Flush();
                }


                //reload all event
                X.AddScript("calendar.fullCalendar( 'refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Saved successfully",
                                     Html = string.Format("Saved new CheckList id: {0}", checkList.Id),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                X.Msg.Alert("error", "error saving CheckList, please fill all data and try again");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error saving new CheckList",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }


            return this.Direct();
        }


        public Ext.Net.MVC.PartialViewResult ShowEventDetailWindow(string containerId, int rowId, string type)
        {
            using (var session = SessionFactory.GetNewSession())
            {
                var model = new EventDetailViewModel();
                model.Employees = session.Query<Employee>().ToList();
                model.Locations = session.Query<Location>().ToList();
                model.Assets = session.Query<Asset>().ToList();

                model.Areas = session.Query<Area>().ToList();
                model.Zones = session.Query<Zone>().ToList();
                model.Statuses = session.Query<Status>().ToList();
                model.Materials = session.Query<Material>().ToList();


                string viewName = null;
                switch (type)
                {
                    case "PlanPreventive":

                        viewName = "PlanPreventiveDetailWindow";
                        model.PlanPreventive = session.Get<PlanPreventive>(rowId);

                        break;

                    case "PlanCheckList":
                        viewName = "PlanCheckListDetailWindow";
                        model.PlanCheckList = session.Get<PlanCheckList>(rowId);
                        break;
                }

                var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.RenderTo)
                               {
                                   ViewName = viewName,
                                   Model = model,
                               };
                return view;
            }
        }


        [HttpPut]
        public DirectResult EditPlanPreventive(PlanPreventive preventive, int rowId)
        {

            try
            {

                var beginDate = Request.Form["BeginDate"].ToDate(DATE_FORMAT);
                var endDate = Request.Form["EndDate"].ToDate(DATE_FORMAT);
                var isAllDay = Request.Form["IsAllDay"].ToBoolean();

                preventive.BeginDate = beginDate.Value;
                preventive.EndDate = endDate;
                preventive.IsAllDay = isAllDay;

                using (var session = SessionFactory.GetNewSession())
                {
                    _log.DebugFormat("employeeId: {0}", preventive.Employee.EmployeeId);
                    _log.DebugFormat("locationId: {0}", preventive.Location.LocationId);
                    _log.DebugFormat("zoneId: {0}", preventive.Zone.ZoneId);

                    _log.DebugFormat("assetId: {0}", preventive.Asset.AssetId);
                    _log.DebugFormat("areaId: {0}", preventive.Area.AreaId);
                    _log.DebugFormat("statusId: {0}", preventive.Status.StatusId);
                    _log.DebugFormat("materialId: {0}", preventive.Material.MaterialId);

                    var dbPreventive = session.Get<PlanPreventive>(rowId);

                    dbPreventive.BeginDate = preventive.BeginDate;
                    dbPreventive.EndDate = preventive.EndDate;
                    dbPreventive.IsAllDay = preventive.IsAllDay;
                    dbPreventive.Detail = preventive.Detail;
                    dbPreventive.Title = preventive.Title;

                    dbPreventive.Area = preventive.Area;
                    dbPreventive.Asset = preventive.Asset;
                    dbPreventive.Employee = preventive.Employee;

                    dbPreventive.Location = preventive.Location;
                    dbPreventive.Material = preventive.Material;
                    dbPreventive.Status = preventive.Status;
                    dbPreventive.Zone = preventive.Zone;

                    session.Flush();
                }

                //reload all event
                X.AddScript("calendar.fullCalendar( 'refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Saved successfully",
                                     Html = string.Format("Preventive id: {0} updated", rowId),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                X.Msg.Alert("error", "Error updating event, please fill all data and try again.");

                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }


            return this.Direct();
        }


        [HttpPut]
        public DirectResult EditPlanCheckList(PlanCheckList checkList, int rowId)
        {

            try
            {

                var beginDate = Request.Form["BeginDate"].ToDate(DATE_FORMAT);
                var endDate = Request.Form["EndDate"].ToDate(DATE_FORMAT);
                var isAllDay = Request.Form["IsAllDay"].ToBoolean();

                checkList.BeginDate = beginDate.Value;
                checkList.EndDate = endDate;
                checkList.IsAllDay = isAllDay;

                using (var session = SessionFactory.GetNewSession())
                {
                    _log.DebugFormat("employeeId: {0}", checkList.Employee.EmployeeId);
                    _log.DebugFormat("locationId: {0}", checkList.Location.LocationId);
                    _log.DebugFormat("zoneId: {0}", checkList.Zone.ZoneId);

                    _log.DebugFormat("areaId: {0}", checkList.Area.AreaId);
                    _log.DebugFormat("statusId: {0}", checkList.Status.StatusId);

                    var dbCheckList = session.Get<PlanCheckList>(rowId);

                    dbCheckList.BeginDate = checkList.BeginDate;
                    dbCheckList.EndDate = checkList.EndDate;
                    dbCheckList.IsAllDay = checkList.IsAllDay;
                    dbCheckList.Detail = checkList.Detail;
                    dbCheckList.Title = checkList.Title;

                    dbCheckList.Area = checkList.Area;
                    dbCheckList.Asset = checkList.Asset;
                    dbCheckList.Employee = checkList.Employee;

                    dbCheckList.Location = checkList.Location;
                    dbCheckList.Status = checkList.Status;
                    dbCheckList.Zone = checkList.Zone;

                    session.Flush();
                }

                //reload all event
                X.AddScript("calendar.fullCalendar('refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Update successfully",
                                     Html = string.Format("CheckList id: {0} updated", rowId),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                X.Msg.Alert("error", "Error updating event, please fill all data and try again.");

                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }

            return this.Direct();
        }

        [HttpDelete]
        public ActionResult DeletePlanPreventive(int rowId)
        {

            try
            {


                using (var session = SessionFactory.GetNewSession())
                {
                    var dbPreventive = session.Get<PlanPreventive>(rowId);

                    session.Delete(dbPreventive);

                    session.Flush();
                }

                var window = X.GetCmp<Window>("PlanPreventiveDetailWindow");
                window.Close();

                //reload all event
                X.AddScript("calendar.fullCalendar( 'refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "delete successfully",
                                     Html = string.Format("Preventive id: {0} deleted", rowId),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                X.Msg.Alert("error", "Error deleting event, please try again.");

                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }

            return this.Direct();
        }

        [HttpDelete]
        public ActionResult DeletePlanCheckList(int rowId)
        {

            try
            {


                using (var session = SessionFactory.GetNewSession())
                {
                    var dbCheckList = session.Get<PlanCheckList>(rowId);

                    session.Delete(dbCheckList);

                    session.Flush();
                }

                var window = X.GetCmp<Window>("PlanCheckListDetailWindow");
                window.Close();

                //reload all event
                X.AddScript("calendar.fullCalendar( 'refetchEvents');");
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "delete successfully",
                                     Html = string.Format("CheckList id: {0} deleted", rowId),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
                X.Msg.Alert("error", "Error deleting event, please try again.");

                _log.Error(ex);
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Error,
                                     Title = "Error",
                                     Html = ex.Message,
                                     AutoHide = true,
                                 }).Show();

            }

            return this.Direct();
        }

        [HttpGet]
        public ActionResult ShowPlanPreventiveReport(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);
            using (var session = SessionFactory.GetNewSession())
            {

                var set = new List<IList<ShowPlanPreventiveReportRecordViewModel>>();
                var assets = session.Query<Asset>().ToList();
                foreach (var asset in assets)
                {
                    var preventiveByAsset = session.Query<PlanPreventive>()
                        .Where(p => p.Asset.AssetId == asset.AssetId)
                        .Select(p =>
                            new ShowPlanPreventiveReportRecordViewModel()
                            {
                                Id = p.Id,
                                AreaName = p.Area.Detail,
                                AssetName = p.Asset.AssetName,
                                BeginDate = p.BeginDate.ToString(DATE_FORMAT),
                                EmployeeName = p.Employee.EmployeeName,
                                EndDate = p.EndDate.HasValue ? p.EndDate.Value.ToString(DATE_FORMAT) : "",
                                IsAllDay = p.IsAllDay,
                                LocationName = p.Location.Name,
                                MaterialName = p.Material.MaterialId,
                                StatusName = p.Status.StatusName,
                                Title = p.Title,
                                ZoneName = p.Zone.ZoneDetail,

                            }).ToList();

                    _log.DebugFormat("preventiveByAsset: {0} count: {1}", asset.AssetName, preventiveByAsset.Count);

                    set.Add(preventiveByAsset);
                }

                view.Model = new ShowPlanPreventiveReportContainerViewModel()
                {
                    PlanPreventives = set.ToArray(),
                    AssetNames = assets.Select(a => a.AssetName).ToArray()
                };
            }

            return view;
        }

        [HttpGet]
        public ActionResult ExportPlanPreventiveReport(int months, int rowId)
        {

            string reportName = "";
            using (var session = SessionFactory.GetNewSession())
            {
                var preventive = session.Get<PlanPreventive>(rowId);
                switch (preventive.Asset.AssetId)
                {
                    case 1: //lamp
                        switch (months)
                        {
                            case 1:
                                reportName = "EmptyForm";
                                break;
                            case 3:
                                reportName = "EmptyForm";
                                break;
                            case 6:
                                reportName = "EmptyForm";
                                break;
                            case 12:
                                reportName = "EmptyForm";
                                break;
                        }

                        break;
                    case 2://air
                        switch (months)
                        {
                            case 1:
                                reportName = "PmAirMonth";
                                break;
                            case 3:
                                reportName = "PmAir3Month";
                                break;
                            case 6:
                                reportName = "PmAir6Month";
                                break;
                            case 12:
                                reportName = "PmAir12Month";
                                break;

                        }

                        break;
                    case 3://elevetor

                        switch (months)
                        {
                            case 1:
                                reportName = "PmElevatorMonth";
                                break;
                            case 3:
                                reportName = "PmElevator3Month";
                                break;
                            case 6:
                                reportName = "PmElevator6Month";
                                break;
                            case 12:
                                reportName = "PmElevator12Month";
                                break;

                        }
                        break;
                }
            }

            X.AddScript("showReport('{0}',{1});", reportName, rowId);
            return this.Direct();
            //return RedirectToAction("Show", "Report", new { reportName = reportName, rowId = rowId });
        }

        public ActionResult ShowPlanCheckListReport(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);

            using (var session = SessionFactory.GetNewSession())
            {

                var set = new List<IList<ShowPlanCheckListReportRecordViewModel>>();
                var assets = session.Query<Asset>().ToList();
                foreach (var asset in assets)
                {
                    var preventiveByAsset = session.Query<PlanCheckList>()
                        .Where(p => p.Asset.AssetId == asset.AssetId)
                        .Select(p =>
                            new ShowPlanCheckListReportRecordViewModel()
                            {
                                Id = p.Id,
                                AreaName = p.Area.Detail,
                                AssetName = p.Asset.AssetName,
                                BeginDate = p.BeginDate.ToString(DATE_FORMAT),
                                EmployeeName = p.Employee.EmployeeName,
                                EndDate = p.EndDate.HasValue ? p.EndDate.Value.ToString(DATE_FORMAT) : "",
                                IsAllDay = p.IsAllDay,
                                LocationName = p.Location.Name,
                                StatusName = p.Status.StatusName,
                                StatusId = p.Status.StatusId,

                                Title = p.Title,
                                ZoneName = p.Zone.ZoneDetail,
                                MaterialName = p.Material.MaterialId


                            }).ToList();

                    _log.DebugFormat("preventiveByAsset: {0} count: {1}", asset.AssetName, preventiveByAsset.Count);

                    set.Add(preventiveByAsset);
                }

                var model = new ShowPlanCheckListReportContainerViewModel()
            {
                CheckLists = set.ToArray(),
                AssetNames = assets.Select(a => a.AssetName).ToArray(),
                Statuses = session.Query<Status>().ToList(),
            };
                model.Statuses.Add(new Status() { StatusId = 0, StatusName = "" });
                view.Model = model;



            }//end session

            return view;

        }

        [HttpGet]
        public ActionResult ExportPlanCheckListReport(int days, int rowId)
        {
            _log.DebugFormat("ExportPlanCheckListReport days: {0}, rowId: {1}", days, rowId);

            string reportName = "";
            using (var session = SessionFactory.GetNewSession())
            {
                var checklist = session.Get<PlanCheckList>(rowId);
                switch (checklist.Asset.AssetId)
                {
                    case 1: //lamp
                        switch (days)
                        {
                            case 1:
                                reportName = "EmptyForm";
                                break;
                            case 7:
                                reportName = "EmptyForm";
                                break;

                        }

                        break;
                    case 2://air
                        switch (days)
                        {

                            case 7:
                                reportName = "ChecklistAirWeek";
                                break;


                        }

                        break;
                    case 3://elevetor

                        switch (days)
                        {

                            case 7:
                                reportName = "ChecklistElevatorIsDay";
                                break;

                        }
                        break;
                }
            }

            X.AddScript("showReport('{0}',{1});", reportName, rowId);
            return this.Direct();

        }

        public ActionResult ShowPlanPreventiveFillableReport(string containerId)
        {
            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);
            using (var session = SessionFactory.GetNewSession())
            {

                var set = new List<IList<ShowPlanPreventiveReportRecordViewModel>>();
                var assets = session.Query<Asset>().ToList();
                foreach (var asset in assets)
                {
                    var preventiveByAsset = session.Query<PlanPreventive>()
                        .Where(p => p.Asset.AssetId == asset.AssetId)
                        .Select(p =>
                            new ShowPlanPreventiveReportRecordViewModel()
                            {
                                Id = p.Id,
                                AreaName = p.Area.Detail,
                                AssetName = p.Asset.AssetName,
                                AssetId = p.Asset.AssetId,
                                BeginDate = p.BeginDate.ToString(DATE_FORMAT),
                                EmployeeName = p.Employee.EmployeeName,
                                EndDate = p.EndDate.HasValue ? p.EndDate.Value.ToString(DATE_FORMAT) : "",
                                IsAllDay = p.IsAllDay,
                                LocationName = p.Location.Name,
                                MaterialName = p.Material.MaterialId,
                                StatusName = p.Status.StatusName,
                                Title = p.Title,
                                ZoneName = p.Zone.ZoneDetail,

                            }).ToList();

                    _log.DebugFormat("preventiveByAsset: {0} count: {1}", asset.AssetName, preventiveByAsset.Count);

                    set.Add(preventiveByAsset);
                }

                view.Model = new ShowPlanPreventiveReportContainerViewModel()
                {
                    PlanPreventives = set.ToArray(),
                    AssetNames = assets.Select(a => a.AssetName).ToArray()
                };
            }

            return view;

        }

        public ActionResult ShowPlanCheckListFillableReport(string containerId)
        {

            var container = X.GetCmp<Container>(containerId);
            container.RemoveAll(true);
            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.AddTo);

            using (var session = SessionFactory.GetNewSession())
            {

                var set = new List<IList<ShowPlanCheckListReportRecordViewModel>>();
                var assets = session.Query<Asset>().ToList();
                foreach (var asset in assets)
                {
                    var preventiveByAsset = session.Query<PlanCheckList>()
                        .Where(p => p.Asset.AssetId == asset.AssetId)
                        .Select(p =>
                            new ShowPlanCheckListReportRecordViewModel()
                            {
                                Id = p.Id,
                                AreaName = p.Area.Detail,
                                AssetName = p.Asset.AssetName,
                                AssetId = p.Asset.AssetId,
                                BeginDate = p.BeginDate.ToString(DATE_FORMAT),
                                EmployeeName = p.Employee.EmployeeName,
                                EndDate = p.EndDate.HasValue ? p.EndDate.Value.ToString(DATE_FORMAT) : "",
                                IsAllDay = p.IsAllDay,
                                LocationName = p.Location.Name,
                                StatusName = p.Status.StatusName,
                                Title = p.Title,
                                ZoneName = p.Zone.ZoneDetail,
                                MaterialName = p.Material.MaterialId


                            }).ToList();

                    _log.DebugFormat("preventiveByAsset: {0} count: {1}", asset.AssetName, preventiveByAsset.Count);

                    set.Add(preventiveByAsset);
                }

                view.Model = new ShowPlanCheckListReportContainerViewModel()
                {
                    CheckLists = set.ToArray(),
                    AssetNames = assets.Select(a => a.AssetName).ToArray()
                };
            }

            return view;
        }

        [HttpPut]
        public DirectResult EditPlanCheckListReport(int id, string field, string oldValue, string newValue)
        {
            try
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new DateTimeConverter("dd/MM/yyyy HH:mm:ss"));
                var planCheckList =
                    JsonConvert.DeserializeObject<ShowPlanCheckListReportRecordViewModel>(Request["planCheckList"], settings);


                using (var session = SessionFactory.GetNewSession())
                {
                    _log.DebugFormat("planCheckList Id: {0}", planCheckList.Id);
                    var planCheckListFromDb = session.Get<PlanCheckList>(planCheckList.Id);
                    planCheckListFromDb.Status = new Status() { StatusId = planCheckList.StatusId };

                    session.Flush();

                    var assetCount = session.Query<Asset>().Count();
                    for (int i = 0; i < assetCount; i++)
                    {
                        var store = X.GetCmp<Store>("planCheckListReportStore" + i);
                        store.CommitChanges();
                    }

                }//end unit of work


                //commit store
                //reload all event
                X.Msg.Notify(new NotificationConfig
                                 {
                                     Icon = Icon.Information,
                                     Title = "Update CheckList status successfully",
                                     Html = string.Format("CheckList id: {0} updated", planCheckList.Id),
                                     AutoHide = true,
                                 }).Show();
            }
            catch (Exception ex)
            {
               var alert= X.Msg.Alert("error", ex.Message);
               alert.Show();

                _log.Error(ex);

            }

            return this.Direct();

        }//end class

    }


    //end class


    //end class

}
