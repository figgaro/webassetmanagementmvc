﻿using System;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Ext.Net;
using Ext.Net.MVC;
using NHibernate.Linq;
using WebAssetManagementMvc.Helpers;
using WebAssetManagementMvc.Models;
using log4net;
using Parameter = Ext.Net.Parameter;

namespace WebAssetManagementMvc.Controllers
{
    public class ReportController : Controller
    {
        private ILog _log = LogManager.GetLogger(typeof(ReportController).Name);
        //private const string DATE_FORMAT = "M/d/yyyy";
        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";

        public ActionResult Show(string reportName, int? rowId)
        {
            var view = new Ext.Net.MVC.PartialViewResult();
            view.ViewName = "ShowReportWindow";
            view.ViewBag.reportName = reportName;
            view.ViewBag.rowId = rowId;
            using (var session = SessionFactory.GetNewSession())
            {
                var assets = session.Query<Asset>().ToList();
                view.ViewBag.assets = assets;
            }

            return view;

        }

        [HttpGet]
        public ActionResult SetLoadReport(string reportName, int? rowId, string beginDate, string endDate,int? assetId)
        {
            _log.DebugFormat("beginDate: {0}", beginDate);
            _log.DebugFormat("endDate: {0}", endDate);

            var panel = X.GetCmp<Panel>("reportContainer");
            var loader = new ComponentLoader
                {
                    Url = Url.Action("GetReport", "Report"),
                    Mode = LoadMode.Frame,
                    LoadMask =
                    {
                        ShowMask = true
                    }
                };
            loader.Params.Add(new Parameter("reportName", reportName));

            if (rowId != null)
            {
                loader.Params.Add(new Parameter("rowId", rowId.Value.ToString()));
            }

            if (!string.IsNullOrEmpty(beginDate))
            {
                loader.Params.Add(new Parameter("beginDate", beginDate));
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                loader.Params.Add(new Parameter("endDate", endDate));
            }

            if (assetId.HasValue)
            {
                loader.Params.Add(new Parameter("assetId", assetId.Value.ToString()));
            }

            panel.LoadContent(loader);
            return this.Direct();
        }


        /// <summary>
        /// this get report with filter
        /// </summary>
        /// <param name="reportName"></param>
        /// <param name="rowId"></param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public ActionResult GetReport(string reportName, int? rowId, string beginDate, string endDate,int? assetId)
        {
            var dataSet = new AssetManagementDataSet();
            var document = new ReportDocument();
            try
            {
                var begin = beginDate.ToDate(DATE_FORMAT);
                var end = endDate.ToDate(DATE_FORMAT);
                _log.DebugFormat("reportName: {0}", reportName);

                _log.DebugFormat("beginDate: {0}", begin);
                _log.DebugFormat("endDate: {0}", end);

                using (var session = SessionFactory.GetNewSession())
                {
                    switch (reportName)
                    {
                        case "rptPlanPm":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var planPreventives = session.Query<PlanPreventive>().ToList();
                                _log.DebugFormat("planPreventives count: {0}", planPreventives.Count());

                                #region filter
                                if (assetId.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.BeginDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.BeginDate <= end.Value).ToList();
                                }
                                #endregion

                                foreach (var item in planPreventives)
                                {
                                    var row = dataSet.PlanPmDataTable.NewRow();
                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["BeginDate"] = item.BeginDate;
                                    row["EndDate"] = item.EndDate;
                                    row["IsAllDay"] = item.IsAllDay;
                                    row["Materail"] = item.Material.MaterialId;
                                    row["Asset"] = item.Asset.AssetName;

                                    row["Area"] = item.Area.AreaId;
                                    row["Location"] = item.Location.Name;
                                    row["Employee"] = item.Employee.EmployeeName;
                                    row["Zone"] = item.Zone.ZoneDetail;

                                    dataSet.PlanPmDataTable.Rows.Add(row);
                                }

                                document.SetDataSource(dataSet);

                            }
                            break;
                        case "rptResultPlanPm":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var planPreventives = session.Query<PlanPreventive>().ToList();
                                _log.DebugFormat("planPreventives result count: {0}", planPreventives.Count());


                                #region filter
                                if (assetId.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.BeginDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    planPreventives = planPreventives.Where(p => p.BeginDate <= end.Value).ToList();
                                }
                                #endregion

                                foreach (var item in planPreventives)
                                {

                                    var row = dataSet.PlanPmDataTable.NewRow();

                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["BeginDate"] = item.BeginDate;
                                    row["EndDate"] = item.EndDate;
                                    row["IsAllDay"] = item.IsAllDay;
                                    row["Materail"] = item.Material.MaterialId;
                                    row["Asset"] = item.Asset.AssetName;

                                    row["Area"] = item.Area.AreaId;
                                    row["Location"] = item.Location.Name;
                                    row["Employee"] = item.Employee.EmployeeName;
                                    row["Zone"] = item.Zone.ZoneDetail;
                                    row["Status"] = item.Status.StatusName;


                                    dataSet.PlanPmDataTable.Rows.Add(row);
                                }

                                document.SetDataSource(dataSet);
                            }
                            break;


                        case "rptPlanChecklist":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var planChecklist = session.Query<PlanCheckList>().ToList();
                                //_log.DebugFormat("planChecklist result count: {0}", planChecklist.Count());


                                #region filter
                                if (assetId.HasValue)
                                {
                                    planChecklist =planChecklist .Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    planChecklist = planChecklist.Where(p => p.BeginDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    planChecklist = planChecklist.Where(p => p.BeginDate <= end.Value).ToList();
                                }
                                #endregion

                                foreach (var item in planChecklist)
                                {
                                    var row = dataSet.PlanClDataTable.NewRow();
                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["BeginDate"] = item.BeginDate;
                                    row["EndDate"] = item.EndDate;
                                    row["IsAllDay"] = item.IsAllDay;
                                    row["Material"] = item.Material.MaterialId;
                                    row["Asset"] = item.Asset.AssetName;

                                    row["Area"] = item.Area.AreaId;
                                    row["Location"] = item.Location.Name;
                                    row["Employee"] = item.Employee.EmployeeName;
                                    row["Zone"] = item.Zone.ZoneDetail;

                                    dataSet.PlanClDataTable.Rows.Add(row);
                                }

                                document.SetDataSource(dataSet);
                            }
                            break;

                        case "rptResultChecklist":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var planChecklist = session.Query<PlanCheckList>().ToList();
                                //_log.DebugFormat("planChecklist result count: {0}", planChecklist.Count());

                                #region filter
                                if (assetId.HasValue)
                                {
                                    planChecklist = planChecklist.Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    planChecklist = planChecklist   .Where(p => p.BeginDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    planChecklist = planChecklist.Where(p => p.BeginDate <= end.Value).ToList();
                                }
                                #endregion
                                foreach (var item in planChecklist)
                                {

                                    var row = dataSet.PlanClDataTable.NewRow();
                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["BeginDate"] = item.BeginDate;
                                    row["EndDate"] = item.EndDate;
                                    row["IsAllDay"] = item.IsAllDay;
                                    row["Material"] = item.Material.MaterialId;
                                    row["Asset"] = item.Asset.AssetName;

                                    row["Area"] = item.Area.AreaId;
                                    row["Location"] = item.Location.Name;
                                    row["Employee"] = item.Employee.EmployeeName;
                                    row["Zone"] = item.Zone.ZoneDetail;
                                    row["Status"] = item.Status.StatusName;

                                    dataSet.PlanClDataTable.Rows.Add(row);
                                }


                                document.SetDataSource(dataSet);
                            }
                            break;

                        case "rptNotifyBD":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var NotifyBD = session.Query<NotifyBreakDown>().ToList();

                                #region filter
                                if (assetId.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.NotifyDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.NotifyDate <= end.Value).ToList();
                                }
                                #endregion

                                foreach (var item in NotifyBD)
                                {

                                    var row = dataSet.NotifyBdDataTable.NewRow();
                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["Material"] = item.Material.MaterialId;
                                    row["Location"] = item.Location.Name;
                                    row["Status"] = item.Status.StatusName;
                                    row["DateNotify"] = item.NotifyDate;
                                    row["Asset"] = item.Asset.AssetName;
                                    row["Area"] = item.Area.AreaId;
                                    row["Zone"] = item.Zone.ZoneDetail;
                                    row["EmpInformant"] = item.EmployeeInformant.EmployeeName;
                                    row["SymptomBefore"] = item.SymptomBefore;

                                    dataSet.NotifyBdDataTable.Rows.Add(row);
                                }

                                document.SetDataSource(dataSet);

                            }
                            break;

                        case "rptResultBreakDown":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                var NotifyBD = session.Query<NotifyBreakDown>().ToList();

                                #region filter
                                if (assetId.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.Asset.AssetId== assetId.Value).ToList();
                                }

                                if (begin.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.NotifyDate >= begin.Value).ToList();
                                }

                                if (end.HasValue)
                                {
                                    NotifyBD = NotifyBD.Where(p => p.NotifyDate <= end.Value).ToList();
                                }
                                #endregion


                                foreach (var item in NotifyBD)
                                {
                                    //var row = dataSet.PlanPmDataTable.NewRow();
                                    var row = dataSet.NotifyBdDataTable.NewRow();
                                    row["ID"] = item.Id;
                                    row["Title"] = item.Title;
                                    row["Material"] = item.Material.MaterialId;
                                    row["Location"] = item.Location.Name;
                                    row["Status"] = item.Status.StatusName;
                                    row["DateNotify"] = item.NotifyDate;
                                    row["DateMintenance"] = item.MaintenanceDate;
                                    row["Asset"] = item.Asset.AssetName;
                                    row["Area"] = item.Area.AreaId;
                                    row["Zone"] = item.Zone.ZoneDetail;
                                    row["EmpInformant"] = item.EmployeeInformant.EmployeeName;
                                    row["EmpBearer"] = item.EmployeeBearer.EmployeeName;

                                    row["SymptomBefore"] = item.SymptomBefore;
                                    row["SymptomAfter"] = item.SymptomAfter;

                                    dataSet.NotifyBdDataTable.Rows.Add(row);
                                }

                                document.SetDataSource(dataSet);
                            }
                            break;




                        //*****************************Report**********************************
                        case "ChecklistAirWeek":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var checklistAirWeek = session.Get<PlanCheckList>(rowId);

                                document.SetParameterValue("No", checklistAirWeek.Id);
                                document.SetParameterValue("ZoneName", checklistAirWeek.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", checklistAirWeek.Location.Name);
                                document.SetParameterValue("Area", checklistAirWeek.Area.AreaId);
                                document.SetParameterValue("Materail", checklistAirWeek.Material.MaterialId);
                                document.SetParameterValue("BeginDate", checklistAirWeek.BeginDate);
                                document.SetParameterValue("EndDate", checklistAirWeek.EndDate);
                                document.SetParameterValue("IsAllDate", checklistAirWeek.IsAllDay);
                                document.SetParameterValue("EmployeeName", checklistAirWeek.Employee.EmployeeName);


                                break;
                            }
                        case "ChecklistElevatorIsDay":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var checklistElevatorIsDay = session.Get<PlanCheckList>(rowId);

                                document.SetParameterValue("No", checklistElevatorIsDay.Id);
                                document.SetParameterValue("ZoneName", checklistElevatorIsDay.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", checklistElevatorIsDay.Location.Name);
                                document.SetParameterValue("Area", checklistElevatorIsDay.Area.AreaId);
                                document.SetParameterValue("Materail", checklistElevatorIsDay.Material.MaterialId);
                                document.SetParameterValue("BeginDate", checklistElevatorIsDay.BeginDate);
                                document.SetParameterValue("EndDate", checklistElevatorIsDay.EndDate);
                                document.SetParameterValue("IsAllDate", checklistElevatorIsDay.IsAllDay);
                                document.SetParameterValue("EmployeeName", checklistElevatorIsDay.Employee.EmployeeName);

                                break;
                            }
                        case "PmAir12Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmAir12Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmAir12Month.Id);
                                document.SetParameterValue("ZoneName", pmAir12Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmAir12Month.Location.Name);
                                document.SetParameterValue("Area", pmAir12Month.Area.AreaId);
                                document.SetParameterValue("Materail", pmAir12Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmAir12Month.BeginDate);
                                document.SetParameterValue("EndDate", pmAir12Month.EndDate);
                                document.SetParameterValue("IsAllDate", pmAir12Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmAir12Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmAir3Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmAir3Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmAir3Month.Id);
                                document.SetParameterValue("ZoneName", pmAir3Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmAir3Month.Location.Name);
                                document.SetParameterValue("Area", pmAir3Month.Area.AreaId);
                                document.SetParameterValue("Materail", pmAir3Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmAir3Month.BeginDate);
                                document.SetParameterValue("EndDate", pmAir3Month.EndDate);
                                document.SetParameterValue("IsAllDate", pmAir3Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmAir3Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmAir6Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var PmAir6Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", PmAir6Month.Id);
                                document.SetParameterValue("ZoneName", PmAir6Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", PmAir6Month.Location.Name);
                                document.SetParameterValue("Area", PmAir6Month.Area.AreaId);
                                document.SetParameterValue("Materail", PmAir6Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", PmAir6Month.BeginDate);
                                document.SetParameterValue("EndDate", PmAir6Month.EndDate);
                                document.SetParameterValue("IsAllDate", PmAir6Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", PmAir6Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmAirMonth":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmAirMonth = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmAirMonth.Id);
                                document.SetParameterValue("ZoneName", pmAirMonth.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmAirMonth.Location.Name);
                                document.SetParameterValue("Area", pmAirMonth.Area.AreaId);
                                document.SetParameterValue("Materail", pmAirMonth.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmAirMonth.BeginDate);
                                document.SetParameterValue("EndDate", pmAirMonth.EndDate);
                                document.SetParameterValue("IsAllDate", pmAirMonth.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmAirMonth.Employee.EmployeeName);

                                break;
                            }

                        case "PmElevator12Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmElevator12Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmElevator12Month.Id);
                                document.SetParameterValue("ZoneName", pmElevator12Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmElevator12Month.Location.Name);
                                document.SetParameterValue("Area", pmElevator12Month.Area.AreaId);
                                document.SetParameterValue("Materail", pmElevator12Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmElevator12Month.BeginDate);
                                document.SetParameterValue("EndDate", pmElevator12Month.EndDate);
                                document.SetParameterValue("IsAllDate", pmElevator12Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmElevator12Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmElevator3Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmElevator3Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmElevator3Month.Id);
                                document.SetParameterValue("ZoneName", pmElevator3Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmElevator3Month.Location.Name);
                                document.SetParameterValue("Area", pmElevator3Month.Area.AreaId);
                                document.SetParameterValue("Materail", pmElevator3Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmElevator3Month.BeginDate);
                                document.SetParameterValue("EndDate", pmElevator3Month.EndDate);
                                document.SetParameterValue("IsAllDate", pmElevator3Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmElevator3Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmElevator6Month":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmElevator6Month = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmElevator6Month.Id);
                                document.SetParameterValue("ZoneName", pmElevator6Month.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmElevator6Month.Location.Name);
                                document.SetParameterValue("Area", pmElevator6Month.Area.AreaId);
                                document.SetParameterValue("Materail", pmElevator6Month.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmElevator6Month.BeginDate);
                                document.SetParameterValue("EndDate", pmElevator6Month.EndDate);
                                document.SetParameterValue("IsAllDate", pmElevator6Month.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmElevator6Month.Employee.EmployeeName);

                                break;
                            }
                        case "PmElevatorMonth":
                            {

                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var pmElevatorMonth = session.Get<PlanPreventive>(rowId);

                                document.SetParameterValue("No", pmElevatorMonth.Id);
                                document.SetParameterValue("ZoneName", pmElevatorMonth.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", pmElevatorMonth.Location.Name);
                                document.SetParameterValue("Area", pmElevatorMonth.Area.AreaId);
                                document.SetParameterValue("Materail", pmElevatorMonth.Material.MaterialId);
                                document.SetParameterValue("BeginDate", pmElevatorMonth.BeginDate);
                                document.SetParameterValue("EndDate", pmElevatorMonth.EndDate);
                                document.SetParameterValue("IsAllDate", pmElevatorMonth.IsAllDay);
                                document.SetParameterValue("EmployeeName", pmElevatorMonth.Employee.EmployeeName);

                                break;
                            }
                        case "BreakdownForm":
                            {
                                document.Load(Server.MapPath(string.Format("~/Reports/{0}.rpt", reportName)));
                                //test how to set parmeter to report
                                //plan preventive
                                var breakdownForm = session.Get<NotifyBreakDown>(rowId);

                                document.SetParameterValue("No", breakdownForm.Id);
                                document.SetParameterValue("ZoneName", breakdownForm.Zone.ZoneDetail);
                                document.SetParameterValue("LocationName", breakdownForm.Location.Name);
                                document.SetParameterValue("Area", breakdownForm.Area.AreaId);
                                document.SetParameterValue("Materail", breakdownForm.Material.MaterialId);
                                document.SetParameterValue("AssetName", breakdownForm.Asset.AssetName);
                                document.SetParameterValue("SymptomBefore", breakdownForm.SymptomBefore);
                                document.SetParameterValue("DataNotify", breakdownForm.NotifyDate);
                                document.SetParameterValue("EmployeeNameNotify",
                                                           breakdownForm.EmployeeInformant.EmployeeName);
                                document.SetParameterValue("EmployeeNameBearer",
                                                           breakdownForm.EmployeeBearer.EmployeeName);

                                break;
                            }


                        //*****************************Form**********************************


                        case "BreakdownForm1":

                            var url = string.Format("/Reports/BreakdownForm1.aspx?rowId={0}", rowId);
                            return Redirect(url);


                        case "ChecklistAirWeek1":

                            var url2 = string.Format("/Reports/ChecklistAirWeek1.aspx?rowId={0}", rowId);
                            return Redirect(url2);

                        case "ChecklistElevatorIsDay1":

                            var url3 = string.Format("/Reports/ChecklistElevatorIsDay1.aspx?rowId={0}", rowId);
                            return Redirect(url3);

                        case "PmAir12Month1":

                            var url4 = string.Format("/Reports/PmAir12Month1.aspx?rowId={0}", rowId);
                            return Redirect(url4);

                        case "PmAir3Month1":

                            var url5 = string.Format("/Reports/PmAir3Month1.aspx?rowId={0}", rowId);
                            return Redirect(url5);

                        case "PmAir6Month1":

                            var url6 = string.Format("/Reports/PmAir6Month1.aspx?rowId={0}", rowId);
                            return Redirect(url6);

                        case "PmAirMonth1":

                            var url7 = string.Format("/Reports/PmAirMonth1.aspx?rowId={0}", rowId);
                            return Redirect(url7);

                        case "PmElevator12Month1":

                            var url8 = string.Format("/Reports/PmElevator12Month1.aspx?rowId={0}", rowId);
                            return Redirect(url8);

                        case "PmElevator3Month1":

                            var url9 = string.Format("/Reports/PmElevator3Month1.aspx?rowId={0}", rowId);
                            return Redirect(url9);


                        case "PmElevator6Month1":

                            var url10 = string.Format("/Reports/PmElevator6Month1.aspx?rowId={0}", rowId);
                            return Redirect(url10);

                        case "PmElevatorMonth1":

                            var url11 = string.Format("/Reports/PmElevatorMonth1.aspx?rowId={0}", rowId);
                            return Redirect(url11);








                    }//end switch  


                } //end  session

            }
            catch (Exception ex)
            {
                _log.Fatal(ex);

            }

            //document.SaveAs("C:/Test/test.pdf");
            //document.ExportToDisk(ExportFormatType.PortableDocFormat, "C:/Test/test.pdf");
            //document.SetParameterValue("projectName", "ทดสอบ");
            //ReportDocument report = document.ExportToStream(ExportFormatType.PortableDocFormat);
            var stream = document.ExportToStream(ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");

        }//end method
    }
}
