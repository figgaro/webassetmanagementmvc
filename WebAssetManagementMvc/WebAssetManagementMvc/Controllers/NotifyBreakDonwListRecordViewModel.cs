using System;

namespace WebAssetManagementMvc.Controllers
{
    public class NotifyBreakDonwListRecordViewModel
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string MaterialName { get; set; }
        public string AssetName { get; set; }
        public string ZoneName { get; set; }

        public string LocationName { get; set; }
        public string AreaName { get; set; }
        public string SymptomBefore { get; set; }
        public string EmployeeInformantName { get; set; }

        public string SymptomAfter { get; set; }
        public int EmployeeBearerId { get; set; }
        public string EmployeeBearerName { get; set; }

        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public string NotifyDate { get; set; }

        public string MaintenanceDate { get; set; }
    }
}