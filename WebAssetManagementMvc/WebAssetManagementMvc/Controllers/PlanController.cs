﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using log4net;

namespace WebAssetManagementMvc.Controllers
{
    public class PlanController : Controller
    {
        //
        // GET: /Plan/
        private ILog _log = LogManager.GetLogger(typeof(PlanController).Name);

        public ActionResult Index()
        {
            return View();
        }


        public Ext.Net.MVC.PartialViewResult ShowPlanWindow(string containerId)
        {

            var view = new Ext.Net.MVC.PartialViewResult(containerId, RenderMode.RenderTo);

            return view;
        }

        [HttpGet]
        public ActionResult GetPlan(string name)
        {

            _log.DebugFormat("name: {0}", name);
            var file = Server.MapPath(string.Format("~/Plans/{0}", name));
            if (!System.IO.File.Exists(file))
            {
                throw new System.IO.FileNotFoundException(string.Format("no file path: {0}", file));
            }

            return File(file, "application/pdf");
        }

    }
}