﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Area
    {
        public virtual string AreaId { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string ZoneId { get; set; }
        public virtual string Detail { get; set; }

    }
}