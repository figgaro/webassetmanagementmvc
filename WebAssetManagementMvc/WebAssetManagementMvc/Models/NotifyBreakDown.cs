﻿using System;

namespace WebAssetManagementMvc.Models
{
    public class NotifyBreakDown
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual Material Material { get; set; }
        public virtual Asset Asset { get; set; }

        public virtual Zone Zone { get; set; }
        public virtual Location Location { get; set; }
        public virtual Area Area { get; set; }
        public virtual string SymptomBefore { get; set; }
        public virtual Employee EmployeeInformant { get; set; }

        public virtual string SymptomAfter { get; set; }
        public virtual Employee EmployeeBearer { get; set; }
        public virtual Status Status { get; set; }
        public virtual DateTime NotifyDate { get; set; }
        public virtual DateTime? MaintenanceDate { get; set; }
        public virtual string FormState { get; set; }

    }
}