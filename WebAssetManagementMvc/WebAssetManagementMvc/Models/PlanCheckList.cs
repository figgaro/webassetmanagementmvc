﻿using System;

namespace WebAssetManagementMvc.Models
{

    public class PlanCheckList
    {
        public virtual int Id { get; set; }



        //public virtual int AssetGroupId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }

        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual bool IsAllDay { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Area Area { get; set; }
        public virtual Zone Zone { get; set; }
        public virtual Location Location { get; set; }
        public virtual Status Status { get; set; }
        public virtual Material Material { get; set; }

        public virtual string FormState { get; set; }
   
    }
}