﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Zone
    {
        public virtual string ZoneId { get; set; }
        public virtual string ZoneDetail { get; set; }
    }
}