﻿namespace WebAssetManagementMvc.Models
{
    public class Employee
    {
        public virtual int EmployeeId { get; set; }
        public virtual string EmployeeName { get; set; }
        public virtual string Address { get; set; }
        public virtual string Sex { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        //public virtual int PositionId { get; set; }
        public virtual Position Position { get; set; }

        //EMPLOYEE_ID
        //POSITION_ID

        //EMPLOYEE_NAME
        //ADDRESS
        //SEX
        //USERNAME
        //PASSWORD

    }
}