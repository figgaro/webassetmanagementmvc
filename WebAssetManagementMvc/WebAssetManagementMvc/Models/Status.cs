﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Status
    {
        public virtual int StatusId { get; set; }
        public virtual string StatusName { get; set; }
    }
}