﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class AssetGroup
    {
        public virtual int AssetGroupId { get; set; }
        public virtual int AssetId { get; set; }
        public virtual string AssetGroupName { get; set; }
        public virtual int AssetGroupCount { get; set; }
    }
}