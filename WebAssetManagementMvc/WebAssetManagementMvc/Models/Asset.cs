﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Asset
    {
        public virtual int AssetId { get; set; }
        public virtual string AssetName { get; set; }
    }
}