﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAssetManagementMvc.Models
{
    public class Position
    {
        public virtual int PositionId { get; set; }
        public virtual string PositionName { get; set; }
        public virtual float Salary { get; set; }
        public virtual IList<Employee> Employees { get; set; }
    }
}