﻿using System;

namespace WebAssetManagementMvc.Models
{
    public class PlanPreventive
    {
        public virtual int Id { get; set; }


        //public virtual int AssetGroupId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual bool IsAllDay { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Location Location { get; set; }
        public virtual Status Status { get; set; }
        public virtual Zone Zone { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Area Area { get; set; }
        public virtual Material Material { get; set; }

        public virtual string FormState { get; set; }

    }
}
//ID
//NO
//SONE_ID
//LOCATION_ID
//AREA_ID
//ASSET_ID
//ASSET_GROUP_ID
//BEGIN_DATE
//DETAIL
//END_DATE
//IS_ALL_DAY
//TITLE
