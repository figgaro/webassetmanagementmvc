﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebAssetManagementMvc.Helpers
{

    public class DateTimeConverter : DateTimeConverterBase
    {

        private readonly string _dateFormat;

        public DateTimeConverter(string dateFormat) 
        {
            _dateFormat = dateFormat;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var format = "{0:" + _dateFormat + "}";
            var dateString = string.Format(format,value);
            writer.WriteValue(dateString);
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

    }

}