﻿using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Event;
using WebAssetManagementMvc.Models;
using Configuration = NHibernate.Cfg.Configuration;

namespace WebAssetManagementMvc
{
    public class SessionFactory
    {
        private static ISessionFactory _sessionFactory;
        public static Configuration Config { get; private set; }


        public static void Init()
        {
            Config = Fluently.Configure()
                    .Database(GetDatabaseConfig())
                    .Mappings(m =>
                    {
                        m.FluentMappings.AddFromAssemblyOf<Employee>();
                        //to use name query in xml file
                        m.HbmMappings.AddFromAssemblyOf<Employee>();
                    })
                    .ExposeConfiguration(TreatConfiguration)
                    .BuildConfiguration();

            _sessionFactory = Config.BuildSessionFactory();
        }

        private static void TreatConfiguration(Configuration cfg)
        {
            cfg.EventListeners.PreInsertEventListeners
            = new IPreInsertEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

            cfg.EventListeners.PreUpdateEventListeners

                 = new IPreUpdateEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

        }

        private static IPersistenceConfigurer GetDatabaseConfig()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
            var databaseConfig = SQLiteConfiguration.Standard.ConnectionString(connectionString);
            return databaseConfig;
        }

        public static ISessionFactory GetSessionFactory()
        {
            if (_sessionFactory == null)
                Init();//we can move this to Global.ascx
            return _sessionFactory;
        }

        public static ISession GetNewSession()
        {
            return GetSessionFactory().OpenSession();
        }


    }//end class
}
