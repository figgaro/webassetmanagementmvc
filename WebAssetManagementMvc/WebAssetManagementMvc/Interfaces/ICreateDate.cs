﻿using System;

namespace WebAssetManagementMvc.Interfaces
{
    public interface ICreateDate
    {
        DateTime CreateDate { get; set; }
    }
}