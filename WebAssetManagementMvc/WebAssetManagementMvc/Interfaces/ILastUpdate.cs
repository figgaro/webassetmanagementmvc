﻿using System;

namespace WebAssetManagementMvc.Interfaces
{
    public interface ILastUpdate
    {
        DateTime LastUpdate { get; set; }
    }
}