﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class MaterialMap:ClassMap<Material>
    {
        public MaterialMap() {

            Table("AM_MATERIAL");

            Id(x => x.MaterialId).Column("MATERAIL_ID").GeneratedBy.Assigned();

            Map(x => x.AssetGroupId).Column("ASSET_GROUP_ID");

        }
    }
}