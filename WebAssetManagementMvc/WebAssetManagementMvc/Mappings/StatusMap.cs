﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class StatusMap:ClassMap<Status>
    {
        public StatusMap() {

            Table("AM_STATUS");
            Id(x => x.StatusId).Column("STATUS").GeneratedBy.Assigned();
            Map(x => x.StatusName).Column("STATUS_NAME"); 
            
        }
    }
}