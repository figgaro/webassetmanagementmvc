﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class ZoneMap:ClassMap<Zone>
    {
        public ZoneMap() {

            Table("AM_ZONE");
            Id(x => x.ZoneId).Column("ID").GeneratedBy.Assigned();
            Map(x => x.ZoneDetail).Column("ZONE_DETAIL");

        }
    }
}