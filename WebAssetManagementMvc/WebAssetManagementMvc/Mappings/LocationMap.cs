﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class LocationMap:ClassMap<Location>
    {

        public LocationMap() {

            Table("AM_LOCATION");

            Id(x => x.LocationId, "ID").GeneratedBy.Assigned();

            Map(x => x.Name, "LOCATION_NAME");

            Map(x => x.Detail, "LOCATION_DETAIL");

                
        }

    }



}