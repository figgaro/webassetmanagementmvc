﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class AssetMap:ClassMap<Asset>
    {
        public AssetMap() {
            Table("AM_ASSET");
            Id(x => x.AssetId).Column("ASSET_ID").GeneratedBy.Assigned();

            Map(x => x.AssetName).Column("ASSET_NAME"); 

        }
    }
}