﻿using FluentNHibernate.Mapping;
using WebAssetManagementMvc.Models;

namespace WebAssetManagementMvc.Mappings
{
    public class PlanCheckListMap:ClassMap<PlanCheckList>
    {

          //ID,  TITLE, BEGIN_DATE, END_DATE, IS_ALL_DATE, ZONE_ID, LACATION_ID, AREA_ID,  EMPLOYEE_ID,STATUS

        public PlanCheckListMap()
        {
            Table("AM_PLAN_CHECKLIST");


            Id(x => x.Id, "ID").Not.Nullable().GeneratedBy.Identity();
            Map(x => x.Title, "TITLE").Not.Nullable();
            Map(x => x.Detail, "DETAIL").Nullable();

            Map(x => x.BeginDate).Column("BEGIN_DATE").Not.Nullable();
            Map(x => x.EndDate).Column("END_DATE").Nullable();
            Map(x => x.IsAllDay).Column("IS_ALL_DAY").Nullable().Default("0");

            References(x => x.Employee).ForeignKey("none").Column("EMPLOYEE_ID");//.Not.Nullable();
            References(x => x.Location).ForeignKey("none").Column("LOCATION_ID");//.Not.Nullable();
            References(x => x.Status).ForeignKey("none").Column("STATUS_ID");//.Not.Nullable();

            References(x => x.Zone).ForeignKey("none").Column("ZONE_ID");//.Not.Nullable();
            References(x => x.Area).ForeignKey("none").Column("AREA_ID");//.Not.Nullable();
            References(x => x.Asset).ForeignKey("none").Column("ASSET_ID");//.Not.Nullable();
            References(x => x.Material).ForeignKey("none").Column("MATERIAL_ID");//.Not.Nullable();

            Map(x => x.FormState).Column("FORM_STATE");

        }
    }
}